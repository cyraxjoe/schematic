stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:

scm.database rec {
    guid = "D0J6PLZEYV46LZA8";
    name = "world";
    server = scm.server rec {
        inherit postgresql;
        guid = "S1LGOZ8QOP2JFGAY";
        name = "world";
        dbname = "world";
        port = "57073";
        user = "root";
        password = "pass";
    };
    dependencies = [
        <language_code-S07PMSDRZWJGSHMZ>
        <country-S0Y2F1PPW4X10VTW>
    ];
}
