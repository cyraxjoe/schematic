import json
import os
import re
import sys

import psycopg2

from schematic import env, build_msg
from schematic.build_util import build_setup


def main():
    build_setup()
    output = env.get_str('output')
    sys.stderr.write('%s\n' % output)
    sys.stderr.flush()
    build_msg.send({
        'type': 'ofveuvekjogy-inspect',
        'output': output,
    })

if __name__ == '__main__':
    main()
