#! /usr/bin/env python3
'''Schematic - PostgreSQL schema control manager.'''

import datetime
import fcntl
import glob
import json
import os
import re
import shutil
import signal
import socket
import stat
import struct
import subprocess
import sys
import termios

import click
import fabric
import invoke
import migra
import pexpect
import pglast
import psutil
import sqlbag
from clint.textui import colored
from toolz import dicttoolz

from schematic import build_msg, dates, env, guids, oplog_ingress
from schematic.build_util import ensure_pg_running, mkdir, pg_ctl_fn, pg_is_ready
from schematic.pguri import pguri_from_basedir

from psycopg2.errors import AdminShutdown
from psycopg2 import OperationalError

@click.group()
def main():
    pass


def eval_module(path, environ=None, verbose=False):
    # sandbox = false is required to read and write to ~/var
    nix_module = get_nix_module(path)
    if not nix_module:
        sys.stderr.write('invalid nix module: %s\n' % path)
        sys.exit(2)
    parallelism = env.get_str('SCM_PARALLELISM', 'auto')
    cmd = [
        'nix-build', get_relative_nix_mod('lib/eval.nix'), '--arg', 'modulepath', nix_module,
        '--no-out-link', '--show-trace', '--option', 'sandbox', 'false', '--max-jobs', parallelism]
    environ = dicttoolz.merge(environ, os.environ) if environ else None
    with subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, text=True, env=environ) as p:
        while True:
            line = p.stdout.readline()
            if not line:
                break
            if verbose:
                sys.stdout.write(line)
                sys.stdout.flush()
            msg = build_msg.read(line)
            if not msg:
                continue
            if msg['type'] == 'rufbemnoilat-please-start':
                if not pg_is_ready(msg['basedir'], msg['port']):
                    pg_ctl_fn(msg['basedir'], 'start')
            yield msg
        p.wait()
        if p.returncode != 0:
            raise subprocess.CalledProcessError(p.returncode, cmd)

def autocomplete_path(ctx, args, incomplete):
    completions = glob.glob('%s*' % incomplete)
    if len(completions) == 1 and os.path.isdir(completions[0]):
        completions = glob.glob('%s/*' % completions[0])
    for c in completions:
        yield c

@main.command()
@click.argument('path', autocompletion=autocomplete_path)
@click.option('-v', '--verbose', envvar='SCM_VERBOSE', is_flag=True)
def upgrade(path, verbose=False):
    '''Create a database and its dependencies or upgrade to latest revision.'''
    msgs = eval_module(path, verbose=verbose)
    for msg in msgs:
        if msg['type'] == 'nojbohishtim-database-ready':
            psql_from_basedir(msg['basedir'])

def get_relative_nix_mod(path, scm_path=env.get_str('SCM_PATH')):
    return os.path.join(scm_path, path)

def _sandbox(path, verbose=False, scm_sandbox_mode='declarative'):
    '''
    Create a temporary sandbox database given a schema, database, or sql file.

    :scm_sandbox_mode: options ('declarative', 'imperative')
        declarative: build using the declarative specifications
        imperative: build using incremental revisions
    '''
    environ = {
        'SCM_PORT': str(get_free_port()),
        'SCM_GUID': guids.get_server_guid(),
        'SCM_SANDBOX_MODE': scm_sandbox_mode,
    }
    nix_module = get_nix_module(path)
    sql_file = get_sql_file(path)
    if sql_file and not nix_module:
        environ['SCM_SANDBOX_SQL'] = os.path.abspath(sql_file)
        nix_module = get_relative_nix_mod('lib/schema-from-sql.nix')
    if not nix_module:
        sys.stderr.write('invalid nix module: %s\n' % path)
        sys.stderr.flush()
        sys.exit(1)
    environ['SCM_SANDBOX_MODULE'] = os.path.abspath(nix_module)
    msgs = eval_module(get_relative_nix_mod('lib/sandbox.nix'), environ=environ, verbose=verbose)
    basedir = None
    ret = []
    try:
        for msg in msgs:
            ret.append(msg)
            if msg['type'] == 'rufbemnoilat-please-start':
                basedir = msg['basedir']
    except subprocess.CalledProcessError:
        _gc_basedir(basedir, stop=True, force=True, verbose=verbose) if basedir else None
        raise
    return ret

def head(iterable):
    for item in iterable:
        return item

def prompt_yesno(prompt, default=False):
    fmt = '%s (y/N) ' if not default else '%s (Y/n) '
    sys.stdout.write(fmt % prompt)
    sys.stdout.flush()
    answer = sys.stdin.readline().strip().lower()
    return {'y': True, 'n': False}.get(answer, default)

@main.command()
@click.argument('path', autocompletion=autocomplete_path)
@click.option('-v', '--verbose', envvar='SCM_VERBOSE', is_flag=True)
@click.option('-m', '--mode', type=click.Choice(['declarative', 'imperative']), default='declarative')
def sandbox(path, mode, verbose=False):
    '''Create a temporary sandbox database given a schema/database/sql file.'''
    try:
        msgs = _sandbox(path, scm_sandbox_mode=mode, verbose=verbose)
    except subprocess.CalledProcessError:
        sys.stderr.write('error: failed to create sandbox database\n')
        sys.stderr.flush()
        sys.exit(1)
    msg = head(msg for msg in msgs if msg['type'] == 'nojbohishtim-database-ready')
    basedir = msg['basedir']
    psql_from_basedir(basedir)
    consent = prompt_yesno('remove sandbox database %s?' % unexpand_user(basedir), default=True)
    if not consent:
        sys.exit(0)
    _gc_basedir(basedir, stop=True, verbose=verbose)

def _inspect(path, verbose=False):
    '''Inspect a schematic module, printing out metadata.'''
    nix_module = get_nix_module(path)
    assert nix_module is not None, 'requires nix_module'
    # sandbox = false is required to read and write to ~/var
    environ = {
        'SCM_RAND': guids.get_deployment_guid(),
    }
    environ = dicttoolz.merge(environ, os.environ) if environ else None
    cmd = [
        'nix-build', get_relative_nix_mod('lib/inspect.nix'), '--arg', 'modulepath', nix_module,
        '--no-out-link', '--show-trace', '--option', 'sandbox', 'false']
    with subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, text=True, env=environ) as p:
        while True:
            line = p.stdout.readline()
            if not line:
                break
            if verbose:
                sys.stdout.write(line)
                sys.stdout.flush()
            msg = build_msg.read(line)
            if not msg:
                continue
            p.wait()
            if p.returncode != 0:
                raise subprocess.CalledProcessError(p.returncode, cmd)
            return json.loads(open(msg['output']).read())

@main.command()
@click.argument('path', autocompletion=autocomplete_path)
@click.option('-v', '--verbose', envvar='SCM_VERBOSE', is_flag=True)
def inspect(path, verbose=False):
    '''Inspect a schematic module, printing out metadata.'''
    data = _inspect(path, verbose)
    print(json.dumps(data, indent=4))

@main.command(name='dependencies-list')
@click.argument('path', autocompletion=autocomplete_path)
@click.option('-t', '--type', type=click.Choice(['schema', 'revision']))
def dependencies_list(path, type, verbose=False):
    ''' Print a list of dependencies for the given object. '''
    seen = set()
    for dep in get_transitive_dependencies(_inspect(path, verbose=False)):
        if type and type != dep['scm_type']:
            continue
        srcpath = dep['source_path']
        if srcpath not in seen:
            print(srcpath)
            seen.add(srcpath)

def parse_anglepath(path):
    '''
    >>> parse_anglepath('<pg_repack>')
    'pg_repack'
    '''
    match = re.match(r'\<(?P<name>.+)\>', path) if path else None
    if match:
        return match.group('name')

def get_nix_module(path, incl_default_nix=False, nix_paths=os.environ['NIX_PATH']):
    '''
    >>> root_dir = env.get_str('ROOT_DIR')
    >>> pkg = os.path.join(root_dir, 'pkg')
    >>> repack = 'pg_repack-HAHOMTIBILACVICS'
    >>> assert get_nix_module('pkg/%s' % repack) == os.path.join(pkg, repack)
    >>> assert get_nix_module('pkg/%s' % repack, incl_default_nix=True) == \
        os.path.join(pkg, '%s/default.nix' % repack)
    >>> assert get_nix_module('<pg_repack-HAHOMTIBILACVICS>') == os.path.join(pkg, repack)
    >>> assert get_nix_module('<foo/pg_repack-HAHOMTIBILACVICS>', nix_paths='foo=%s' % pkg) == \
            os.path.join(pkg, repack)
    '''
    anglepath = parse_anglepath(path)
    if anglepath:
        for nix_path in nix_paths.split(':'):
            if '=' in nix_path:
                prefix_str, _, sub = nix_path.partition('=')
                prefix_list = prefix_str.split(os.path.sep)
                path_list = anglepath.split(os.path.sep)
                if prefix_list == path_list[:len(prefix_list)]:
                    path = os.path.join(sub, *path_list[len(prefix_list):])
                    break
            else:
                _path = os.path.join(nix_path, anglepath)
                if os.path.exists(_path):
                    path = _path
                    break
    path = os.path.expanduser(os.path.normpath(path))
    if not os.path.isabs(path):
        path = os.path.abspath(path)
    default_nix_path = os.path.join(path, 'default.nix')
    if path.endswith('.nix') and os.path.exists(path):
        return path
    elif os.path.isdir(path) and os.path.exists(default_nix_path):
        if incl_default_nix:
            return default_nix_path
        return path.rstrip('/')

def get_sql_file(path):
    if path.endswith('.sql') and os.path.exists(path):
        return path

def get_free_port():
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.bind(('', 0))
    addr = s.getsockname()
    s.close()
    return addr[1]

def ls_basedirs(store=os.environ['SCM_PG']):
    return glob.glob(os.path.join(store, '*'))

def ls_tmuxservers(scm_tmux=os.environ['SCM_TMUX']):
    return glob.glob(os.path.join(scm_tmux, '*'))

def unexpand_user(path, home=os.environ['HOME']):
    '''
    >>> unexpand_user('/home/scott/var/pg', home='/home/scott')
    '~/var/pg'
    '''
    return re.sub('^%s/' % home.rstrip('/'), '~/', path) if path else path

@main.command()
@click.option('-v', '--verbose', envvar='SCM_VERBOSE', is_flag=True)
def status(verbose):
    '''List status of all databases in ~/var/pg.'''
    for basedir in ls_basedirs():
        pidfile = os.path.join(basedir, 'data/postmaster.pid')
        pid = int(open(pidfile).readlines()[0].strip()) if os.path.exists(pidfile) else None
        metajson = os.path.join(basedir, 'meta.json')
        try:
            metadata = json.loads(open(metajson).read())
        except (json.JSONDecodeError, KeyError):
            sys.stderr.write('invalid %s\n' % unexpand_user(metajson)) if verbose else None
            continue
        except FileNotFoundError:
            sys.stderr.write('missing %s\n' % unexpand_user(metajson)) if verbose else None
            continue
        try:
            proc = psutil.Process(pid) if pid else None
        except psutil.NoSuchProcess:
            proc = None
        out = ''
        out += ('pid %5d' % pid) if pid else '         '
        if proc:
            out += (' %s %s' % (colored.green('up'), dates.fmt_age(proc.create_time())))
        else:
            stat = os.stat(basedir)
            out += (' %s %s' % (colored.red('dn'), dates.fmt_age(stat.st_mtime)))
        out += ' port %5d' % metadata['port']
        out += ' %s' % unexpand_user(basedir)
        print(out)
    sys.stderr.flush()

def get_basedir(path):
    basedir = os.path.normpath(os.path.expanduser(path))
    if basedir.endswith('/data'):
        basedir = '/'.join(basedir.split('/')[:-1])
    return basedir

def autocomplete_basedir(ctx, args, incomplete):
    prefix = os.environ['SCM_PG'].rstrip('/')
    basedirs = glob.glob('%s/*' % prefix)
    basedirs = basedirs + [unexpand_user(d) for d in basedirs]
    return [d for d in basedirs if d.startswith(incomplete)]

@main.command()
@click.argument('basedir', autocompletion=autocomplete_basedir, nargs=-1)
@click.option('--shutdown-mode', '-m', type=click.Choice(['smart', 'fast', 'immediate']), default='fast', help='Specifies the shutdown mode. Default: fast') # pylint: disable=line-too-long
@click.option('-q', '--quiet', is_flag=True, help='Quiet mode. Updates only the log file. No output is shown.')
def stop(basedir, shutdown_mode, quiet):
    '''Stop specified database server.'''
    for bdir in basedir:
        basedir = get_basedir(bdir)
        pg_ctl_fn(bdir, 'stop', shutdown_mode=shutdown_mode, quiet=quiet)

@main.command()
@click.argument('basedir', autocompletion=autocomplete_basedir, nargs=-1)
@click.option('-q', '--quiet', is_flag=True, help='Quiet mode. Updates only the log file. No output is shown.')
def start(basedir, quiet):
    '''Start specified database server.'''
    for bdir in basedir:
        basedir = get_basedir(bdir)
        pg_ctl_fn(bdir, 'start', quiet=quiet)

@main.command()
@click.argument('basedir', autocompletion=autocomplete_basedir, nargs=-1)
@click.option('-q', '--quiet', is_flag=True, help='Quiet mode. Updates only the log file. No output is shown.')
def reload(basedir, quiet):
    '''Reload specified database server.
        This allows changing of configuration-file options that do not require a complete restart to take effect.'''
    for bdir in basedir:
        basedir = get_basedir(bdir)
        pg_ctl_fn(bdir, 'reload', quiet=quiet)

@main.command()
@click.argument('basedir', autocompletion=autocomplete_basedir, nargs=-1)
@click.option('--shutdown-mode', '-m', type=click.Choice(['smart', 'fast', 'immediate']), default='fast', help='Specifies the shutdown mode. Default: fast') # pylint: disable=line-too-long
@click.option('-q', '--quiet', is_flag=True, help='Quiet mode. Updates only the log file. No output is shown.')
def restart(basedir, shutdown_mode, quiet):
    '''Restart specified database server. This allows changing the postgres command-line options.'''
    for bdir in basedir:
        basedir = get_basedir(bdir)
        pg_ctl_fn(bdir, 'restart', shutdown_mode=shutdown_mode, quiet=quiet)

def psql_from_basedir(basedir):
    metajson = os.path.join(basedir, 'meta.json')
    metadata = json.loads(open(metajson).read())
    ensure_pg_running(basedir, metadata['port'])
    signal.signal(signal.SIGINT, signal.SIG_IGN)
    psql_cmd = [
        os.path.join(basedir, 'bin/psql'), '-p', str(metadata['port']), '-U', metadata['user'], metadata['dbname']]
    subprocess.Popen(psql_cmd).communicate()

@main.command()
@click.argument('basedir', autocompletion=autocomplete_basedir)
def psql(basedir):
    '''Open psql shell to specified database.'''
    basedir = get_basedir(basedir)
    psql_from_basedir(basedir)

def _gc_basedir(basedir, stop=False, force=False, verbose=False):
    if not basedir:
        return
    metajson = os.path.join(basedir, 'meta.json')
    metadata = None
    try:
        metadata = json.loads(open(metajson).read())
    except (json.JSONDecodeError, KeyError):
        sys.stderr.write('invalid %s\n' % unexpand_user(metajson)) if verbose else None
        sys.stderr.flush()
        return
    except FileNotFoundError:
        sys.stderr.write('missing %s\n' % unexpand_user(metajson)) if verbose else None
        sys.stderr.flush()
        return
    if not metadata['istemp'] and not force:
        sys.stderr.write(
            'not a temp datababase: %s (hint: use `scm gc-basedir --force` to insist on deleting)\n' %
            unexpand_user(basedir)) if verbose else None
        sys.stderr.flush()
        return
    pidfile = os.path.join(basedir, 'data/postmaster.pid')
    is_running = os.path.exists(pidfile)
    if not metadata['istemp'] and (stop or not is_running):
        consent = prompt_yesno('remove database %s?' % unexpand_user(basedir))
        if not consent:
            return
    if is_running:
        if not stop:
            sys.stdout.write(
                'won\'t gc a running server: %s  (hint: use `scm gc-basedir --stop` to insist on stopping)\n' %
                unexpand_user(basedir)) if verbose else None
            sys.stdout.flush()
            return
        pg_ctl_fn(basedir, 'stop', shutdown_mode='immediate')
    sys.stderr.write('removing %s\n' % unexpand_user(basedir))
    subprocess.check_call(['chmod', '-R', '0700', basedir])
    shutil.rmtree(basedir)

@main.command(name='gc-basedir')
@click.argument('basedir', autocompletion=autocomplete_basedir)
@click.option('-s', '--stop', is_flag=True, help='Stop database if it\'s running (otherwise leave it be).')
@click.option('-f', '--force', is_flag=True, help='Prompt before removing a non-sandbox database.')
def gc_basedir(basedir, stop, force):
    _gc_basedir(basedir, stop=stop, force=force, verbose=True)

def is_socket(path):
    s = os.stat(path)
    return stat.S_ISSOCK(s.st_mode)

def _gc(verbose):
    for basedir in ls_basedirs():
        _gc_basedir(basedir, verbose=verbose)
    for socket_path in ls_tmuxservers():
        if not is_socket(socket_path):
            continue
        remove_tmux_socket(socket_path)
    sys.stderr.flush()

@main.command()
@click.option('-v', '--verbose', envvar='SCM_VERBOSE', is_flag=True)
def gc(verbose):
    '''
    Delete obsolete sandbox databases from ~/var/pg and detached sockets from ~/var/tmux/.

    Database must be stopped and specify istemp = true in meta.json.
    '''
    _gc(verbose)

def create_schema(name):
    guid = guids.get_schema_guid()
    dirname = '%s-%s' % (name, guid)
    schema_path = os.path.abspath(os.path.join(env.get_str('ROOT_DIR'), 'pkg', dirname))
    mkdir(schema_path)
    nixout = os.path.join(schema_path, 'default.nix')
    if os.path.exists(nixout):
        sys.stderr.write('error, file exists: %s\n' % nixout)
        sys.exit(1)
    with open(nixout, 'w') as out:
        out.write('''
stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:

scm.schema {
    guid = "%s";
    name = "%s";
    upgrade_sql = ./upgrade.sql;
    dependencies = [

    ];
}\n'''.lstrip() % (guid, name))
    sqlout = os.path.join(schema_path, 'upgrade.sql')
    if os.path.exists(sqlout):
        sys.stderr.write('error, file exists: %s\n' % sqlout)
        sys.exit(1)
    with open(sqlout, 'w') as out:
        out.write('\n')
    print('created "<%s>"' % dirname)
    print('\t%s' % os.path.relpath(nixout))
    print('\t%s' % os.path.relpath(sqlout))
    return '<%s>' % dirname

@main.command()
@click.argument('name')
def schema(name):
    '''Create files for a new schema object.'''
    create_schema(name)

def path_relative(path, relativeto):
    '''Turn an absolute path reference into a path to a reference path.'''
    relpath = os.path.relpath(path, relativeto)
    if not relpath.startswith('/') and not relpath.startswith('.'):
        relpath = './%s' % relpath
    return relpath

def path_nixpath(path, nix_paths=os.environ['NIX_PATH']):
    '''Turn an absolute path reference into a path relative to NIX_PATH.'''
    if not nix_paths:
        return
    abs_path = os.path.normpath(os.path.expanduser(path))
    basename = os.path.basename(path)
    for nix_path in nix_paths.split(':'):
        if '=' in nix_path:
            prefix_str, _, sub = nix_path.partition('=')
            sub_list = sub.split(os.path.sep)
            path_list = abs_path.split(os.path.sep)
            if sub_list == path_list[:len(sub_list)]:
                return '<%s/%s>' % (prefix_str, os.path.sep.join(path_list[len(sub_list):]))
        elif os.path.relpath(abs_path, nix_path) == basename:
            return '<%s>' % basename

def path_norm(path, relativeto=None, nix_paths=os.environ['NIX_PATH']):
    '''
    Turn an absolute path reference into a path relative to NIX_PATH or relative to a reference path.

    >>> root_dir = env.get_str('ROOT_DIR')
    >>> rev = os.path.join(root_dir, 'rev/2020-11-16-country-R000GHY2HI7C7675')
    >>> pkg = os.path.join(root_dir, 'pkg/country-S0Y2F1PPW4X10VTW')
    >>> srv = os.path.join(root_dir, 'srv/world-D0J6PLZEYV46LZA8')
    >>> path_norm(srv, None, os.path.join(root_dir, 'srv'))
    '<world-D0J6PLZEYV46LZA8>'
    >>> path_norm(srv, None, 'srv=%s' % os.path.join(root_dir, 'srv'))
    '<srv/world-D0J6PLZEYV46LZA8>'
    >>> path_norm(rev, None, os.path.join(root_dir, 'rev'))
    '<2020-11-16-country-R000GHY2HI7C7675>'
    >>> path_norm(rev, None, 'rev=%s' % os.path.join(root_dir, 'rev'))
    '<rev/2020-11-16-country-R000GHY2HI7C7675>'
    >>> path_norm(pkg, None, os.path.join(root_dir, 'pkg'))
    '<country-S0Y2F1PPW4X10VTW>'
    >>> path_norm(pkg, None, 'pkg=%s' % os.path.join(root_dir, 'pkg'))
    '<pkg/country-S0Y2F1PPW4X10VTW>'
    >>> path_norm(srv, pkg, None)
    '../../srv/world-D0J6PLZEYV46LZA8'
    >>> path_norm(rev, pkg, None)
    '../../rev/2020-11-16-country-R000GHY2HI7C7675'
    >>> path_norm(pkg, srv, None)
    '../../pkg/country-S0Y2F1PPW4X10VTW'
    '''
    return path_nixpath(path, nix_paths) or path_relative(path, relativeto)

def update_schema_deps(schema_path, dependencies):
    '''Replace the 'dependencies' attribute in a schema source file.'''
    schema_nix = get_nix_module(schema_path, incl_default_nix=True)
    schema_dir = os.path.dirname(schema_nix)
    orig_src = open(schema_nix).read()
    norm_deps = [path_norm(d, schema_dir) for d in dependencies]
    def replacement(match):
        return 'dependencies = [%s\n    ];' % ''.join(['\n        %s' % d for d in sorted(norm_deps, reverse=True)])
    updated_src = re.sub(r'dependencies\s*=\s*\[\s*([\w\./\-_\s\<\>]*)\s*\];', replacement, orig_src, flags=re.DOTALL)
    with open(schema_nix, 'w') as out:
        out.write(updated_src)
    updated_schema_metadata = _inspect(schema_nix)
    updated_norm_deps = [path_norm(d['source_path'], schema_dir) for d in updated_schema_metadata['dependencies']]
    if set(norm_deps) == set(updated_norm_deps):
        print('updated %s' % os.path.relpath(schema_nix))
    else:
        sys.stderr.write('failed updating %s - must update dependencies by hand\n' % os.path.relpath(schema_nix))
        with open(schema_nix, 'w') as out:
            out.write(orig_src)

def get_transitive_dependencies(inspect_metadata):
    for dep in inspect_metadata['dependencies'] if inspect_metadata else []:
        yield dep
        yield from get_transitive_dependencies(dep)

def get_transitive_revision_heads(inspect_metadata):
    for dep in inspect_metadata['dependencies'] if inspect_metadata else []:
        if dep['scm_type'] == 'revision':
            yield dep
        if dep['scm_type'] == 'schema':
            yield from get_transitive_revision_heads(dep)

def get_minimal_revision_heads(inspect_metadata):
    ''' Get the mininum set of revisions dependended on (eliminating redundant transitive dependencies). '''
    suppress = set()
    deps = list(get_transitive_revision_heads(inspect_metadata))
    for dep in deps:
        for trans_dep in get_transitive_dependencies(dep):
            suppress.add(trans_dep['source_path'])
    for dep in deps:
        if dep['source_path'] not in suppress:
            yield dep

def get_immediate_revision_heads(inspect_metadata):
    for dep in inspect_metadata['dependencies'] if inspect_metadata else []:
        if dep['scm_type'] == 'revision':
            yield dep

def get_transitive_schemas(inspect_metadata):
    ''' Yield dependent schemas, depth-firsth. '''
    for dep in inspect_metadata['dependencies'] if inspect_metadata else []:
        if dep['scm_type'] == 'schema':
            yield from get_transitive_schemas(dep)
            yield dep

def get_transitive_schemas_unique(inspect_metadata):
    ''' Yield dependent schemas, depth-firsth, de-duplicated. '''
    seen = set()
    for dep in get_transitive_schemas(inspect_metadata):
        srcpath = dep['source_path']
        if srcpath not in seen:
            seen.add(srcpath)
            yield dep

def _revision_diff_filesystem(schema_basefiles, rev_basefiles, obj_basedir, sbj_basedir, verbose=False):
    '''
    If the schema has basefiles defined, diff the declarative against the imperative basedir to discover basefiles
    that have diverged.

    This is useful for discovering basefiles that have been edited in a schema so that they can be included in a
    generated revision.

    Returns list of relative files names that have been changed.
    '''
    ret = []
    for path in walk_dir_rel(rev_basefiles):
        rev_path = os.path.join(rev_basefiles, path)
        if os.path.isdir(rev_path):
            if not os.listdir(rev_path):
                os.rmdir(rev_path)
            continue
        obj_path = os.path.join(obj_basedir, path) if obj_basedir else None
        sbj_path = os.path.join(sbj_basedir, path) if sbj_basedir else None
        if same_file_content(obj_path, sbj_path):
            sys.stderr.write('same %s\n' % path) if verbose else None
            sys.stderr.flush()
            os.unlink(rev_path)
        else:
            ret.append(path)
            sys.stderr.write('diff %s\n' % path) if verbose else None
            sys.stderr.flush()
    if not os.listdir(rev_basefiles):
        os.rmdir(rev_basefiles)
    return ret

def create_revision(path, verbose=False, auto=False):
    schema_path = get_nix_module_dir(path)
    if not schema_path:
        sys.stderr.write('invalid nix module: %s\n' % path)
        sys.exit(2)
    schema_metadata = _inspect(schema_path)
    schema_name = schema_metadata['name']
    guid = guids.get_revision_guid()
    name = '%s-%s' % (datetime.datetime.now().date().isoformat(), schema_name)
    dirname = '%s-%s' % (name, guid)
    revision_dir = os.path.join(env.get_str('ROOT_DIR'), 'rev', dirname)
    mkdir(revision_dir)
    print('created "<%s>"' % dirname)
    revision_heads = list(get_minimal_revision_heads(schema_metadata))
    dependencies_str = '\n        '.join(
        sorted(list(set([path_norm(r['source_path'], revision_dir) for r in revision_heads])), reverse=True))
    revision_nix = os.path.join(revision_dir, 'default.nix')
    revision_content = ''
    obj_basedir = sbj_basedir = None
    do_diff = auto and bool(revision_heads or schema_metadata['basefiles'])
    if do_diff:
        try:
            obj_msgs = _sandbox(schema_path, scm_sandbox_mode='declarative', verbose=verbose)
            sbj_msgs = _sandbox(schema_path, scm_sandbox_mode='imperative', verbose=verbose)
        except subprocess.CalledProcessError:
            sys.stderr.write('error: failed to create sandbox database\n')
            sys.stderr.flush()
            sys.exit(1)
        for msg in obj_msgs:
            if msg['type'] == 'nojbohishtim-database-ready':
                obj_basedir = msg['basedir']
        for msg in sbj_msgs:
            if msg['type'] == 'nojbohishtim-database-ready':
                sbj_basedir = msg['basedir']
    if not list(get_immediate_revision_heads(schema_metadata)):
        # prefer a literal copy of upgrade.sql for initial revision (over diff)
        revision_content = open(os.path.join(schema_path, 'upgrade.sql')).read()
    elif obj_basedir and sbj_basedir:
        migration = schema_diff(sbj_basedir, obj_basedir)
        revision_content = migration.sql
    schema_basefiles = os.path.join(schema_path, 'basefiles')
    rev_basefiles = os.path.join(revision_dir, 'basefiles')
    include_basefiles = False
    if os.path.isdir(schema_basefiles):
        include_basefiles = True
        subprocess.check_call(['rsync', '-rlc', '%s/' % schema_basefiles, rev_basefiles])
        if do_diff and obj_basedir and sbj_basedir:
            include_basefiles = bool(
                _revision_diff_filesystem(schema_basefiles, rev_basefiles, obj_basedir, sbj_basedir, verbose))
    revision_nix_content = '''
stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:

scm.revision {
    guid = "%(guid)s";
    name = "%(name)s";
    basefiles = ./basefiles;
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        %(dependencies)s
    ];
}\n'''.lstrip() % {'guid': guid, 'name': name, 'dependencies': dependencies_str}
    if not include_basefiles:
        revision_nix_content = revision_nix_content.replace('''
    basefiles = ./basefiles;''', '')
    with open(revision_nix, 'w') as out:
        out.write(revision_nix_content)
    print('\t%s' % os.path.relpath(revision_nix))
    upgrade_sql = os.path.join(revision_dir, 'upgrade.sql')
    with open(upgrade_sql, 'w') as out:
        out.write(revision_content)
    print('\t%s' % os.path.relpath(upgrade_sql))
    schema_new_deps = (
        [d['source_path']
            for d in (schema_metadata['dependencies'] if schema_metadata else [])
            if d['scm_type'] != 'revision'] + [revision_dir])
    update_schema_deps(schema_path, schema_new_deps)
    _gc_basedir(obj_basedir, force=True, stop=True, verbose=verbose)
    _gc_basedir(sbj_basedir, force=True, stop=True, verbose=verbose)

@main.command()
@click.argument('schema_path', autocompletion=autocomplete_path)
@click.option('-v', '--verbose', envvar='SCM_VERBOSE', is_flag=True)
@click.option('-a', '--auto', is_flag=True, help='Initialize a suggested revision from diff (WIP).')
@click.option('-r', '--recursive', is_flag=True, help='Also create revisions for dependency schemas without any.')
def revision(schema_path, verbose, auto, recursive):
    '''Create files for a new revision object to migrate the specified schema.'''
    if recursive:
        for dep in get_transitive_schemas_unique(_inspect(schema_path)):
            dep_schema_path = path_norm(dep['source_path'])
            if not any(d['scm_type'] == 'revision' for d in dep['dependencies']):
                create_revision(dep_schema_path, verbose=verbose, auto=auto)
    create_revision(schema_path, verbose=verbose, auto=auto)

@main.command(name='lint-dependencies')
@click.argument('path', autocompletion=autocomplete_path, nargs=-1)
def lint_dependencies(path):
    '''Sort and de-duplicate 'dependencies' list in nix modules.'''
    for p in path or []:
        try:
            metadata = _inspect(p)
        except AssertionError:
            metadata = None
        if not metadata:
            sys.stderr.write('%s: invalid nix module?\n' % p)
            sys.stderr.flush()
            continue
        deps = [d['source_path'] for d in metadata['dependencies']]
        if deps != sorted(deps, reverse=True):
            sys.stderr.write('%s: sorted\n' % p)
        uniq_deps = sorted(list(set(deps)), reverse=True)
        if len(deps) != len(uniq_deps):
            sys.stderr.write('%s: uniqued %d -> %d\n' % (p, len(deps), len(uniq_deps)))
        if deps == uniq_deps:
            continue
        sys.stderr.flush()
        update_schema_deps(p, uniq_deps)

def file_content(filename):
    '''Return file mode bits and file content in tuple (for equivalence comparison).'''
    try:
        return os.stat(filename).st_mode, open(filename).read()
    except FileNotFoundError:
        return (None, None)

def same_file_content(file1, file2):
    '''Check if the two filenames contain contain the same content and have the same file mode bits.'''
    data1 = file_content(file1)
    data2 = file_content(file2)
    return data1 == data2

def walk_dir_rel(prefix):
    '''Generate relative sub-paths of @prefix from bottom up.'''
    for dirpath, dirnames, filenames in os.walk(prefix, topdown=False):
        for path in dirnames + filenames:
            yield os.path.relpath(os.path.join(dirpath, path), prefix)

@main.command()
@click.argument('name')
@click.option('-d', '--dbname', help='Define postgresql dbname (if different from name).')
def database(name, dbname):
    '''
    Create files for a new database object.

    name: Logical name of database (tip: suffix primaries with 0, replicas with 1, 2, etc).

    dbname: Physical name of database (passed to PostgreSQL createdb).
    '''
    dbname = dbname or name
    database_guid = guids.get_database_guid()
    dirname = '%s-%s' % (name, database_guid)
    database_path = os.path.abspath(os.path.join(env.get_str('ROOT_DIR'), 'srv', dirname))
    server_guid = guids.get_server_guid()
    mkdir(database_path)
    outpath = os.path.join(database_path, 'default.nix')
    if os.path.exists(outpath):
        sys.stderr.write('error, file exists: %s\n' % outpath)
        sys.exit(1)
    params = {
        'database_guid': database_guid,
        'server_guid': server_guid,
        'name': name,
        'dbname': dbname,
        'port': get_free_port(),
        'user': env.get_str('SCM_USER', 'root'),
        'pass': env.get_str('SCM_PASS', 'pass'),
    }
    with open(outpath, 'w') as out:
        out.write('''
stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:

scm.database rec {
    guid = "%(database_guid)s";
    name = "%(name)s";
    server = scm.server rec {
        inherit postgresql;
        guid = "%(server_guid)s";
        name = "%(name)s";
        dbname = "%(dbname)s";
        port = "%(port)d";
        user = "%(user)s";
        password = "%(pass)s";
    };
    dependencies = [

    ];
}\n'''.lstrip() % params)
    print('created %s' % outpath)


class ScmMigration(migra.Migration):

    def __init__(self, x_from, x_target, schema=None):
        super(ScmMigration, self).__init__(x_from, x_target, schema=schema)
        self.set_safety(False)

    def add_creations(self, privileges=False):
        self.add(self.changes.schemas(creations_only=True))
        self.add(self.changes.extensions(creations_only=True))
        self.add(self.changes.collations(creations_only=True))
        self.add(self.changes.enums(creations_only=True, modifications=True))
        self.add(self.changes.sequences(creations_only=True))
        # This command doesn't support creations_only; can generate DROP TABLE statements
        self.add(self.changes.selectables())
        self.add(self.changes.indexes(creations_only=True))
        self.add(self.changes.pk_constraints(creations_only=True))
        self.add(self.changes.non_pk_constraints(creations_only=True))
        self.add(self.changes.privileges(creations_only=True)) if privileges else None
        self.add(self.changes.rlspolicies(creations_only=True))
        self.add(self.changes.triggers(creations_only=True))

def _get_pg_uri(ref):
    ''' Get postgresql URI from a basedir path or postgresql URI. '''
    if not ref:
        return
    if ref.lower().startswith('postgresql://'):
        return ref
    return pguri_from_basedir(get_basedir(ref))

def schema_diff(obj, sbj):
    '''Diff two databases (specified by basedir) and output the schema difference.'''
    obj_db = get_basedir(obj)
    sbj_db = get_basedir(sbj)
    obj_uri = _get_pg_uri(obj)
    sbj_uri = _get_pg_uri(sbj)
    with sqlbag.S(str(obj_uri)) as obj_pg, sqlbag.S(str(sbj_uri)) as sbj_pg:
        migration = ScmMigration(obj_pg, sbj_pg)
        migration.add_creations()
        # HACK: remove me once migra is replaced with custom diff
        blacklist = ['drop table "meta"."revision";']
        for item in blacklist:
            if item in migration.statements:
                migration.statements.remove(item)
        return migration

@main.command()
@click.argument('declarative', autocompletion=autocomplete_basedir)
@click.argument('imperative', autocompletion=autocomplete_basedir)
def diff(declarative, imperative):
    '''Diff two databases (specified by basedir or postgresql URI) and output the schema difference.'''
    # TODO: should also diff files in basedir
    migration = schema_diff(declarative, imperative)
    if migration.statements:
        print(migration.sql)
    else:
        sys.stderr.write('nothing to do\n')

def get_pg_dump(pguri, tablename):
    cmd = [
        'pg_dump', '--no-owner', '--schema-only', '--no-privileges',
        '-d', str(pguri),
        '-t', tablename,
    ]
    return subprocess.check_output(cmd, text=True)

def parse_ddl_statements(text):
    statement = ''
    in_comment_block = False
    for line in text.split('\n'):
        if line.startswith('/*'):
            in_comment_block = True
        if in_comment_block:
            if line.endswith('*/'):
                in_comment_block = False
            continue
        if line.startswith('--') or line.startswith('SET ') or line.startswith('SELECT pg_catalog.set_config'):
            continue
        if not line.strip():
            continue
        statement += line + '\n'
        if line.endswith(';'):
            yield statement
            statement = ''

def parse_create_table_names(text):
    # TODO: upgrade pglast to use pg12 and replace this implementation
    for statement in parse_ddl_statements(text):
        match = re.match(r'CREATE( UNLOGGED)? TABLE (?P<tablename>(\w+\.)\w+)\W', statement)
        if match:
            yield match.group('tablename')

def find_statement_match(src_text, statement):
    if statement in src_text:
        return statement
    statement = re.sub(r'\s+', ' ', statement).rstrip(' ')
    if statement in src_text:
        return statement

def get_nix_module_dir(module):
    path = get_nix_module(module)
    if path:
        return path if os.path.isdir(path) else os.path.dirname(path)

def _schemaname(tablename):
    '''
    >>> _schemaname('public.foo')
    'foo'
    >>> _schemaname('bar.baz')
    'bar.baz'
    '''
    nameparts = tablename.split('.')
    if len(nameparts) == 2 and nameparts[0] == 'public':
        return nameparts[-1]
    return tablename

def safe_get(data, *keys):
    '''
    Access @*keys in nested @data dictionary and return `None` instead of `KeyError`.

    >>> safe_get(None, 'x')
    >>> safe_get({}, 'x')
    >>> safe_get({'x': 1}, 'x')
    1
    >>> safe_get({'x': {'y': 2}}, 'x', 'y')
    2
    >>> safe_get({'x': {'y': ''}}, 'x', 'y')
    ''
    >>> safe_get({'x': [{'y': 'z'}]}, 'x', 0, 'y')
    'z'
    >>> safe_get({'x': [{'y': 'z'}]}, 'x', '0', 'y')
    >>> safe_get(['a'], 1)
    '''
    val = data
    for key in keys:
        if val is None:
            return
        try:
            val = val[key]
        except (TypeError, IndexError, KeyError):
            val = None
    return val

def _parse_fk_ref_table(statement):
    '''
    >>> _parse_fk_ref_table('ALTER TABLE ONLY public.f34 ADD CONSTRAINT sdf FOREIGN KEY (f34256) REFERENCES public.fj349_r435h(id) NOT VALID;')
    'public.fj349_r435h'
    '''
    p = pglast.parse_sql(statement)
    ref = safe_get(
        p, 0, 'RawStmt', 'stmt', 'AlterTableStmt', 'cmds', 0, 'AlterTableCmd', 'def', 'Constraint', 'pktable',
        'RangeVar')
    if ref:
        if 'schemaname' in ref:
            return '%s.%s' % (ref['schemaname'], ref['relname'])
        return ref['relname']

def _get_fk_reference_module(fk_statement, nix_paths=os.environ['NIX_PATH']):
    tableref = _parse_fk_ref_table(fk_statement)
    if not tableref:
        return
    files = file_search(r'CREATE\s+.*TABLE\s+%s\W' % tableref, nix_paths.split(':'), 'sql', case_sensitive=False)
    for file_match in files:
        mod_name = os.path.dirname(file_match)
        mod_metadata = _inspect(mod_name)
        if mod_metadata and mod_metadata.get('scm_type') == 'schema':
            return mod_name

def _is_create_trigger(statement):
    '''
    >>> _is_create_trigger('CREATE TRIGGER w AFTER INSERT ON q.z FOR EACH ROW EXECUTE FUNCTION x.y();')
    True
    '''
    # TODO: upgrade pglast to use pg12 and replace this implementation
    return bool(re.match(r'\s*CREATE\s+TRIGGER\s+.*', statement or '', flags=re.IGNORECASE))

def _is_create_index(statement):
    '''
    >>> _is_create_index('CREATE INDEX revision_date_idx ON meta.revision USING btree (date);')
    True
    '''
    p = pglast.parse_sql(statement)
    return bool(safe_get(p, 0, 'RawStmt', 'stmt', 'IndexStmt'))

def _is_add_constraint(statement):
    '''
    >>> _is_add_constraint('ALTER TABLE country ADD CONSTRAINT country_id_upper CHECK (id = upper(id));')
    True
    '''
    p = pglast.parse_sql(statement)
    for cmd in safe_get(p, 0, 'RawStmt', 'stmt', 'AlterTableStmt', 'cmds') or []:
        if safe_get(cmd, 'AlterTableCmd', 'def', 'Constraint'):
            return True
    return False

def _eq_pglast_name(name, struct):
    '''
    >>> _eq_pglast_name('public.wombat', [{'String': {'str': 'public'}}, {'String': {'str': 'wombat'}}])
    True
    >>> _eq_pglast_name('wombat', [{'String': {'str': 'public'}}, {'String': {'str': 'wombat'}}])
    True
    >>> _eq_pglast_name('foo.wombat', [{'String': {'str': 'public'}}, {'String': {'str': 'wombat'}}])
    False
    >>> _eq_pglast_name('foo.wombat', [{'String': {'str': 'wombat'}}])
    True
    '''
    np1 = name.split('.')[::-1]
    np2 = [safe_get(x, 'String', 'str') for x in struct or []][::-1]
    return all([x == y for (x, y) in zip(np1, np2)])

def _is_create_func_table_arg(statement, table):
    '''
    >>> _is_create_func_table_arg('CREATE FUNCTION public.squawk(r public.wombat) RETURNS text LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    True
    >>> _is_create_func_table_arg('CREATE FUNCTION public.squawk(public.wombat) RETURNS text LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    True
    >>> _is_create_func_table_arg('CREATE FUNCTION public.squawk(public.wombat, foo) RETURNS text LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    True
    >>> _is_create_func_table_arg('CREATE FUNCTION public.squawk(x public.wombat, foo) RETURNS text LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    True
    >>> _is_create_func_table_arg('CREATE FUNCTION public.squawk(foo, public.wombat) RETURNS text LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    True
    >>> _is_create_func_table_arg('CREATE FUNCTION public.squawk(foo, x public.wombat) RETURNS text LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    True
    >>> _is_create_func_table_arg('CREATE FUNCTION public.squawk(integer) RETURNS text LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    False
    '''
    try:
        p = pglast.parse_sql(statement)
    except pglast.parser.ParseError as ex:
        # TODO: fix parser in pglast; happens when $ is used inside a function body to refer to a positional argument
        sys.stderr.write(('failed to parse %s' % (statement.replace('\n', '')))[:120] + '\n')
        sys.stderr.flush()
        return
    for param in safe_get(p, 0, 'RawStmt', 'stmt', 'CreateFunctionStmt', 'parameters') or []:
        if _eq_pglast_name(table, safe_get(param, 'FunctionParameter', 'argType', 'TypeName', 'names')):
            return True
    return False

def _is_create_func_table_ret(statement, table):
    '''
    >>> _is_create_func_table_ret('CREATE FUNCTION public.squawk() RETURNS public.wombat LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    True
    >>> _is_create_func_table_ret('CREATE FUNCTION public.squawk(integer) RETURNS text LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    False
    >>> _is_create_func_table_ret('CREATE FUNCTION public.squawk() RETURNS integer LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    False
    >>> _is_create_func_table_ret('CREATE FUNCTION public.squawk(integer) RETURNS wombat LANGUAGE sql AS $_$ SELECT 1; $_$;', 'public.wombat')
    True
    '''
    try:
        p = pglast.parse_sql(statement)
    except pglast.parser.ParseError as ex:
        # TODO: fix parser in pglast; happens when $ is used inside a function body to refer to a positional argument
        sys.stderr.write(('failed to parse %s' % (statement.replace('\n', '')))[:120] + '\n')
        sys.stderr.flush()
        return
    return _eq_pglast_name(table, safe_get(p, 0, 'RawStmt', 'stmt', 'CreateFunctionStmt', 'returnType', 'TypeName', 'names'))

def _find_table_funcs(src_text, table):
    '''
    Return create function statements whose type signature depends on the given table.
    '''
    for ddl_statement in parse_ddl_statements(src_text):
        if _is_create_func_table_arg(ddl_statement, table) or _is_create_func_table_ret(ddl_statement, table):
            yield ddl_statement

def _factorize_log_statement(prefix, statement):
    sys.stderr.write(('%s: %s' % (prefix, statement.replace('\n', '')))[:120] + '\n')
    sys.stderr.flush()

@main.command()
@click.argument('schema', autocompletion=autocomplete_path)
@click.option('-t', '--table', help='Regexp search for server to attach with -a, or the name of a new server.')
@click.option('-r', '--revision', is_flag=True, help='Create initial revisions for new schemas.')
@click.option('--keep-failed', is_flag=True, help='Keep changes even when a schema fails to build.')
def factorize(schema, table, revision, keep_failed):
    '''
    Create one new schema per table present in @schema, removing tables and related objects from schema's upgrade.sql.

    WARNING: mutates source schema's upgrade.sql, ensure that it's backed up.

    Removes the table definitions and related objects from the schema's declarative definition, creates a new schema
    with these definitions, and adds a dependency to the new schema.

    This "factorize" utility is a proof of concept, pre-alpha.

    TODO: would save a bunch of manual work if it automatically determined dependencies between the new schemas. This
    would most likely be done by inspecting the catalog tables of the sandbox database created for the parent schema.
    '''
    upgrade_sql = os.path.join(get_nix_module_dir(schema), 'upgrade.sql')  # TODO: replace hard-coded 'upgrade.sql'
    schema_metadata = _inspect(schema)
    schema_deps = [d['source_path'] for d in schema_metadata['dependencies'] or []]
    tablenames = [table] if table else list(parse_create_table_names(open(upgrade_sql).read()))
    try:
        msgs = _sandbox(schema, scm_sandbox_mode='declarative', verbose=False)
    except subprocess.CalledProcessError:
        sys.stderr.write('%s: schema build failed\n' % schema)
        sys.stderr.flush()
        sys.exit(0)
    msg = head(msg for msg in msgs if msg['type'] == 'nojbohishtim-database-ready')
    sandbox_uri = pguri_from_basedir(msg['basedir'])
    for tablename in tablenames:
        if not tablename.strip():
            continue
        statement_matches = []
        failed_match = did_funcs = False
        src_text = open(upgrade_sql).read()
        for ddl_statement in parse_ddl_statements(get_pg_dump(sandbox_uri, tablename)):
            if _is_create_trigger(ddl_statement):
                '''
                Two reasons for skipping triggers:
                1. They depend on functions which aren't included in the pg_dump output for a given table (this could
                be worked around).
                2. There's a judgement call for which schema trigger might belong, so skipping leaves it to a human.
                '''
                continue
            if not did_funcs and (_is_create_index(ddl_statement) or _is_add_constraint(ddl_statement)):
                # constraints and indices may depend on functions, let's add the functions first
                for func_statement in _find_table_funcs(src_text, tablename):
                    statement_matches.append(func_statement)
                    _factorize_log_statement('MATCHED', func_statement)
                did_funcs = True
            match_statement = find_statement_match(src_text, ddl_statement)
            if not match_statement:
                failed_match = True
                _factorize_log_statement('UNMATCHED', ddl_statement)
                continue
            _factorize_log_statement('MATCHED', match_statement)
            statement_matches.append(match_statement)
        if failed_match and not keep_failed:
            sys.stderr.write('skipping %s because of failed statement matches\n' % tablename)
            continue
        if statement_matches:
            newschema = create_schema(_schemaname(tablename))
            newschema_deps = []
            with open(os.path.join(get_nix_module_dir(newschema), 'upgrade.sql'), 'a') as new_upgrade_sql:
                for sttmnt in statement_matches:
                    new_upgrade_sql.write(sttmnt)
                    if not sttmnt.endswith('\n'):
                        new_upgrade_sql.write('\n')
                    src_text = src_text.replace(sttmnt, '')
                    fk_ref_module = _get_fk_reference_module(sttmnt)
                    if fk_ref_module:
                        if get_nix_module(fk_ref_module) != get_nix_module(schema):
                            _factorize_log_statement('INFERRED', '%s depends on %s' % (newschema, fk_ref_module))
                            newschema_deps.append(fk_ref_module)
            if newschema_deps:
                update_schema_deps(get_nix_module(newschema), newschema_deps)
            # build new schema
            new_basedir = None
            try:
                msgs = _sandbox(newschema, scm_sandbox_mode='declarative', verbose=False)
                msg = head(msg for msg in msgs if msg['type'] == 'nojbohishtim-database-ready')
                new_basedir = msg['basedir']
            except subprocess.CalledProcessError:
                sys.stderr.write('%s: schema build failed\n' % newschema)
            create_revision(newschema, verbose=True, auto=False) if new_basedir and revision else None
            if new_basedir or keep_failed:
                # update default.nix to add a dependency on the new schema
                schema_deps.append(get_nix_module(newschema))
                update_schema_deps(get_nix_module(schema), schema_deps)
                # update upgrade.sql to remove the extracted statements
                with open(upgrade_sql, 'w') as fp:
                    fp.write(src_text)
            shutil.rmtree(get_nix_module_dir(newschema)) if not new_basedir and not keep_failed else None
            _gc_basedir(new_basedir, stop=True) if new_basedir else None

def nix_path_list(nix_path=os.environ['NIX_PATH']):
    ret = []
    for path in nix_path.split(':'):
        if '=' in path:
            _, _, path = path.partition('=')
        ret.append(path)
    return ret

def file_search(pattern, paths, extension, case_sensitive=True):
    paths = list(set(paths))
    cmd = ['grep', '-Erl', '--include', "*.%s" % extension]
    if not case_sensitive:
        cmd = cmd + ['-i']
    try:
        output = subprocess.check_output(cmd + [pattern] + paths, text=True)
    except subprocess.CalledProcessError as ex:
        if ex.returncode != 1:
            raise
        return []
    for line in output.split('\n'):
        if os.path.isfile(line):
            yield line

def replace_path_name(source_name, dest_name, content):
    '''
    >>> source_name = '2020-03-17-kighluinbimfoobi-account'
    >>> dest_name = 'account-2020-03-17-kighluinbimfoobi'
    >>> replace_path_name(source_name, dest_name, 'dependencies = [ <2020-03-17-kighluinbimfoobi-account> ]; ')
    'dependencies = [ <account-2020-03-17-kighluinbimfoobi> ]; '
    >>> replace_path_name(source_name, dest_name, 'dependencies = [ <rev/2020-03-17-kighluinbimfoobi-account> ]; ')
    'dependencies = [ <rev/account-2020-03-17-kighluinbimfoobi> ]; '
    >>> replace_path_name(source_name, dest_name, 'dependencies = [ ./rev/2020-03-17-kighluinbimfoobi-account ]; ')
    'dependencies = [ ./rev/account-2020-03-17-kighluinbimfoobi ]; '
    >>> replace_path_name(source_name, dest_name, 'name = "2020-03-17-kighluinbimfoobi-account"; ')
    'name = "account-2020-03-17-kighluinbimfoobi"; '
    >>> replace_path_name(source_name, dest_name, 'name = "2020-03-17-kighluinbimfoobi-account"; ')
    'name = "account-2020-03-17-kighluinbimfoobi"; '
    '''
    content = re.sub(r'\<%s\>' % source_name, '<%s>' % dest_name, content)
    content = re.sub(r'\<((.+/))%s\>' % source_name, lambda m: '<%s%s>' % (m.group(1), dest_name), content)
    content = re.sub(r'"%s"' % source_name, '"%s"' % dest_name, content)
    content = re.sub(r'/%s(\s)' % source_name, '/' + dest_name + r'\1', content)
    return content

@main.command()
@click.argument('source', autocompletion=autocomplete_path)
@click.argument('dest', autocompletion=autocomplete_path)
@click.option('-v', '--verbose', envvar='SCM_VERBOSE', is_flag=True)
def mv(source, dest, verbose):
    '''Move object from @source to @dest and update references. Updates source files destructively!'''
    assert os.path.isdir(source), 'source must be a directory: %s' % source
    source = source.rstrip('/')
    dest = dest.rstrip('/')
    source_name = os.path.basename(source)
    dest_name = os.path.basename(dest)
    os.rename(source, dest)
    for filename in file_search('\W%s\W' % source_name, nix_path_list(), 'nix'):
        oldcontent = content = open(filename).read()
        content = replace_path_name(source_name, dest_name, content)
        if oldcontent == content:
            continue
        try:
            with open(filename, 'w') as fp:
                fp.write(content)
            sys.stderr.write('updated %s\n' % filename)
        except PermissionError as ex:
            if ex.errno == 13:
                sys.stderr.write('readonly %s\n' % filename)
                continue
            raise

@main.command(name='fast-forward')
@click.argument('database', autocompletion=autocomplete_path)
@click.argument('path', autocompletion=autocomplete_path)
@click.option('-v', '--verbose', envvar='SCM_VERBOSE', is_flag=True)
@click.option('-f', '--force', is_flag=True, help='Force marking revision are applied, skipping validation.')
def fast_forward(database, path, verbose, force):
    '''
    Upgrade a database by skipping revisions and marking them as already applied.

    Takes the target database path followed by a revision to fast-forward to. A schema or database can be passed
    instead of a revision to specify all of its dependent revisions.

    Unless --force is specified, a sandbox database will be built to diff against, and the command will fail if the
    database doesn't have the expected schema.
    '''
    if not force:
        # TODO: diff data as well
        try:
            msgs = _sandbox(path, scm_sandbox_mode='imperative', verbose=verbose)
        except subprocess.CalledProcessError:
            sys.stderr.write('error: failed to create sandbox database\n')
            sys.stderr.flush()
            sys.exit(1)
        sandbox_basedir = None
        for msg in msgs:
            if msg['type'] == 'nojbohishtim-database-ready':
                sandbox_basedir = msg['basedir']
                break
        db_meta = _inspect(database, verbose)
        assert db_meta['scm_type'] == 'database', '%s is a %s, expected a database' % (database, db_meta['scm_type'])
        migration = schema_diff(sandbox_basedir, db_meta['server']['basedir'])
        if migration.statements:
            sys.stderr.write('The database does not have compatible schema. Diff:\n\n')
            sys.stderr.write(migration.sql)
            sys.exit(1)
    db_module = get_nix_module(database)
    ff_module = get_nix_module(path)
    assert ff_module is not None, 'not a nix module: %s' % ff_module
    assert db_module is not None, 'not a nix module: %s' % db_module
    environ = {
        'SCM_DATABSE_MODULE': db_module,
        'SCM_FAST_FORWARD_MODULE': ff_module,
        'SCM_FAST_FORWARD': '1',
        'SCM_FAST_FORWARD_GUID': guids.get_database_guid(),
    }
    msgs = list(eval_module(get_relative_nix_mod('lib/fast-forward.nix'), verbose=verbose, environ=environ))

def ensure_nix_installed(fabconn, hostname, installer=os.path.join(env.get_str('SCM_PATH'), 'shell/nix-install.sh')):
    is_installed = bool(fabconn.run('~/.nix-profile/bin/nix-shell --version', hide=True).stdout.strip())
    if is_installed:
        return
    subprocess.check_call(['rsync', installer, '%s:~/var/deploy/nix-install.sh' % hostname])
    try:
        fabconn.run('sh ~/var/deploy/nix-install.sh')
    except invoke.exceptions.UnexpectedExit:
        sys.stderr.write('failed to install nix, please install and try again\n')
        sys.stderr.flush()
        sys.exit(2)

def status_line(line):
    sys.stderr.write('\033[0K\r')
    sys.stderr.write(line)
    sys.stderr.flush()

@main.command()
@click.argument('hostname')
@click.option('-a', '--attach', is_flag=True, help='Attached to most recent existing tmux server.')
@click.option('-n', '--name', help='Regexp search for server to attach with -a, or the name of a new server.')
@click.option('-s', '--shell', default='default.nix', help='Nix file to use for nix-shell environment.')
def remote(hostname, attach, name, shell):
    '''Sync files to host and open a tmux session to adminster remote databases.'''
    status_line('ssh...')
    c = fabric.Connection(hostname)
    c.run('mkdir -p ~/var/{deploy,tmux}', hide=True)
    if attach:
        status_line('lookup tmux server...')
        socket = c.run('ls -t ~/var/tmux/ | grep -E "%s" | head -n 1' % (name or '')).stdout.strip()
        if not socket:
            status_line('tmux server not found\n')
            sys.exit(1)
    else:
        status_line('rsync...')
        subprocess.check_call([
            'rsync', '-zlrt', '--delete-after', 'pkg', 'rev', 'srv', shell, '%s:~/var/deploy' % hostname])
        socket = '-'.join(filter(None, [datetime.datetime.now().date().isoformat(), name, guids.get_deployment_guid()]))
    socket = os.path.join('~/var/tmux', socket)
    status_line('nix install...')
    ensure_nix_installed(c, hostname)

    def resize_window(sig=None, data=None):
        (rows, cols, _, _) = struct.unpack('hhhh', fcntl.ioctl(
            sys.stdout.fileno(), termios.TIOCGWINSZ, struct.pack("HHHH", 0, 0, 0, 0)))
        child.setwinsize(rows, cols)

    child = pexpect.spawn('ssh %s' % hostname)
    signal.signal(signal.SIGWINCH, resize_window)
    child.expect('$', timeout=120)
    child.sendline('cd ~/var/deploy ; export SCM_SOCK=%s' % socket)
    status_line('building nix shell...')
    child.sendline('nix-shell %s --run "tmux -S $SCM_SOCK new -A -s 0 ; scm kill-tmux -p $SCM_SOCK" ; exit' % shell)
    resize_window()
    child.expect(r'\[0\]', timeout=60 * 60 * 24 * 365)
    child.interact()

def tmux_server_is_dead(socket_path):
    try:
        return not subprocess.check_output(['lsof', socket_path]).strip()
    except subprocess.CalledProcessError:
        return True

def tmux_server_clients(socket_path):
    try:
        output = subprocess.check_output(['tmux', '-S', socket_path, 'list-clients'], text=True, stderr=subprocess.PIPE)
        return [l.strip() for l in output.split('\n') if l.strip()]
    except subprocess.CalledProcessError:
        return []

def file_open_pids(filepath):
    '''Get pids that have a file open.'''
    try:
        output = subprocess.check_output(['lsof', '-F', 'p', filepath], text=True, stderr=subprocess.PIPE)
    except subprocess.CalledProcessError:
        return []
    pids = []
    for line in output.split('\n'):
        match = re.match(r'p(\d+)', line)
        if match:
            pids.append(int(match.group(1)))
    return pids

def remove_tmux_socket(socket_path):
    if tmux_server_is_dead(socket_path):
        sys.stderr.write('removing socket %s\n' % unexpand_user(socket_path))
        sys.stderr.flush()
        try:
            subprocess.check_call(['rm', socket_path])
        except subprocess.CalledProcessError:
            pass

@main.command(name='kill-tmux')
@click.argument('socket_path', autocompletion=autocomplete_path)
@click.option('-p', '--prompt', is_flag=True, help='Prompt for confirmation before killing processes.')
@click.option('-f', '--force', is_flag=True, help='Kill even if there are attached clients.')
def kill_tmux(socket_path, prompt, force):
    '''Kill and clean up a tmux server.'''
    if not os.path.exists(socket_path):
        sys.stderr.write('file not found %s\n' % socket_path)
        sys.stderr.flush()
        sys.exit(1)
    clients = tmux_server_clients(socket_path)
    if clients and not force:
        sys.stderr.write('%d client(s) still attached to tmux server %s\n' % (len(clients), unexpand_user(socket_path)))
        sys.stderr.flush()
        sys.exit(2)
    if prompt and not tmux_server_is_dead(socket_path):
        consent = prompt_yesno('kill tmux server %s?' % unexpand_user(socket_path))
        if not consent:
            sys.exit(0)
    pids = file_open_pids(socket_path)
    if pids:
        subprocess.check_call(['kill'] + list(map(str, pids)))
    remove_tmux_socket(socket_path)



def _unique_list(thelist):
    suppress = set()
    for item in thelist:
        if item in suppress:
            continue
        suppress.add(item)
        yield item

@main.command(name='mknixpath')
def mknixpath():
    ''' Read SCM_REPOS env var, iterate sub-directories and output a value for NIX_PATH. '''
    scm_repos = env.get_str('SCM_REPOS', '')
    nix_paths = []
    for scm_repo in scm_repos.split(':'):
        if not scm_repo:
            continue
        if not os.path.exists(scm_repo):
            print('repo does not exist: %s' % scm_repo, file=sys.stderr)
            continue
        scm_repo = os.path.realpath(scm_repo)
        if not os.path.isdir(scm_repo):
            print('repo is not a directory: %s' % scm_repo, file=sys.stderr)
            continue
        for subpath in os.listdir(scm_repo):
            abspath = os.path.join(scm_repo, subpath)
            if os.path.isdir(abspath):
                nix_paths.append(abspath)
    nix_path = env.get_str('NIX_PATH')
    if nix_path:
        nix_paths.extend(nix_path.split(':'))
    print(':'.join(list(_unique_list(nix_paths))))

@main.command(name='oplog-ls')
@click.argument('s3uri')
def ol_ls(s3uri):
    ''' List available oplog files.
        :s3uri: eg s3://some-bucket/some-prefix. Lists all files following the path specified.
    '''
    for olf in oplog_ingress.ls(s3uri):
        print('%12.d bytes    %s    %s' % (olf.size, olf.modified, olf.path))

@main.command('oplog-ingress')
@click.argument('s3uri')
@click.argument('pguri')
@click.option('--replication-role', help='Sets PostgreSQL session_replication_role setting.', type=click.Choice(['replica', 'origin', 'local']), default='replica')  # pylint: disable=line-too-long
def ol_ingress(s3uri, pguri, replication_role):
    ''' Receive oplog update feed into postgresql.
        :s3uri: eg s3://some-bucket/some-prefix
        :pguri: eg postgresql://user:pass@localhost:port/dbname?application_name=oplog-ingress
    '''
    try:
        oplog_ingress.ingress(s3uri, pguri, replication_role=replication_role)
    except (AdminShutdown, OperationalError) as exc:
        sys.stderr.write('error: %s\n' % type(exc).__name__)
        for arg in exc.args:
            sys.stderr.write('%s\n' % arg)
        sys.stderr.flush()
        exit(1)

if __name__ == '__main__':
    main()
