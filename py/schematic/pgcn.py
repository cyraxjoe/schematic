from contextlib import contextmanager

import psycopg2


class Cursor(psycopg2.extensions.cursor):

    def scalar(self):
        if self.rowcount == 0:
            return
        assert self.rowcount <= 1, 'scalar() expects a single row result'
        assert len(self.description) == 1, 'scalar() expects a single column result'
        return self.fetchone()[0]

class PGConn:

    def __init__(self, conn):
        self.conn = conn
        self.cur = None

    def execute(self, query, vars=None):
        if self.cur:
            self.cur.close()
        self.cur = self.conn.cursor()
        self.cur.execute(query, vars)
        return self.cur

    def close(self):
        if self.cur:
            self.cur.close()
        self.conn.close()

    def commit(self):
        self.conn.commit()

class Unset:
    pass

@contextmanager
def connect(pguri, readonly=Unset, autocommit=Unset):
    '''Open a psycopg2 postgresql connection with a context manager.'''
    pg = None
    try:
        pguri = pguri + ('&' if '?' in pguri else '?') +  'application_name=scm'
        conn = psycopg2.connect(pguri, cursor_factory=Cursor)
        if readonly is not Unset:
            conn.set_session(readonly=readonly)
        if autocommit is not Unset:
            conn.set_session(autocommit=autocommit)
        pg = PGConn(conn)
        yield pg
    finally:
        pg.close() if pg else None
