'''Utilities to support python nix builder scripts.'''

import datetime
import functools
import json
import os
import subprocess
import sys
import time

from schematic import dates, build_msg, env

def mkdir(path):
    try:
        os.makedirs(path)
    except OSError as ex:
        if ex.errno not in (17,):
            raise

def build_setup():
    proc = subprocess.Popen([os.path.join(env.get_str('bash'), 'bin/bash'), '-c', 'source $stdenv/setup && env'],
                            stdout=subprocess.PIPE)
    for line in proc.stdout:
        (key, _, value) = line.decode('utf8').partition('=')
        os.environ[key] = value
    proc.communicate()
    mkdir(env.get_str('out'))

def retry(attempts, exceptions=None, onerror=None):
    '''Decorator to retry a function on error.'''

    def outer(func):
        @functools.wraps(func)
        def inner(*args, **kwargs):
            for attempt in range(attempts):
                try:
                    return func(*args, **kwargs)
                except exceptions:
                    if onerror is not None:
                        onerror(*args, **kwargs)
                    if attempt >= attempts - 1:
                        raise
        return inner
    return outer

def pg_is_ready(basedir, port):
    cmd = [os.path.join(basedir, 'bin/pg_isready'), '-d', 'postgres', '-p', str(port)]
    try:
        subprocess.check_call(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        return True
    except subprocess.CalledProcessError:
        return False

def poll_pg_isready(basedir, port, attempts=10):
    for _ in range(attempts):
        if pg_is_ready(basedir, port):
            return True
        time.sleep(1)
    return False

def pg_ctl_fn(basedir, mode, shutdown_mode='fast', quiet=False):
    pg_ctl = os.path.join(basedir, 'bin/pg_ctl')
    if not os.path.exists(pg_ctl):
        print('error: bin/pg_ctl not found in %s' % (basedir), file=sys.stderr)
        if os.path.exists(os.path.join(basedir, 'default.nix')):
            print('Perhaps you meant to pass a path from %s' % env.get_str('SCM_PG', '~/var/pg'), file=sys.stderr)
        sys.exit(19)

    data_dir = os.path.join(basedir, 'data')
    pg_ctl_log = os.path.join(data_dir, 'pg_ctl.log')
    cmd = [pg_ctl, mode, '-D', data_dir, '-l', pg_ctl_log]
    out = subprocess.PIPE if quiet else None
    environ = {}
    if mode in ['stop', 'restart']:
        cmd.extend(['-m', shutdown_mode])
        environ = {
            # this ensures that children don't inherit postmaster's oom_score_adj
            'PG_OOM_ADJUST_FILE': os.environ.get('PG_OOM_ADJUST_FILE', '/proc/self/oom_score_adj'),
        }
    try:
        subprocess.check_call(cmd, stdout=out, stderr=out, env=environ)
    except subprocess.CalledProcessError as ex:
        if ex.returncode != 1:
            raise

def ensure_pg_running(basedir, port):
    if not pg_is_ready(basedir, port):
        pg_ctl_fn(basedir, 'start')
    poll_pg_isready(basedir, port)

def request_pg_start(basedir, port):
    if pg_is_ready(basedir, port):
        return
    build_msg.send({
        'type': 'rufbemnoilat-please-start',
        'basedir': basedir,
        'port': port,
    })
    poll_pg_isready(basedir, port)

def install_basedir_files(basedir, *derivs):
    for deriv in filter(None, derivs):
        if not os.path.exists(deriv):
            continue
        if deriv.endswith('-basefiles'):
            path = deriv
        elif os.path.exists(os.path.join(deriv, 'basefiles')):
            path = os.path.join(deriv, 'basefiles')
        else:
            continue
        sys.stderr.write('installing %r -> %r\n' % (path, basedir))
        # TODO: more sophisticated policy about copying/overwriting files into database
        subprocess.check_call(['rsync', '-rlc', '%s/' % path, basedir])

def install_buildinput_files():
    install_basedir_files(
        env.get_str('basedir'), env.get_str('out'), env.get_str('basefiles'), *env.get_array('buildInputs'))

def apply_sql_file(pg, obj_path):
    content = open(obj_path, encoding='utf8').read()
    if not content.strip():
        return
    empty_line = lambda l: not l or not l.strip() or l.lstrip().startswith('--')
    if all(empty_line(l) for l in content.splitlines()):
        return
    pg.execute(content)

def write_meta_json():
    with open(os.path.join(env.get_str('basedir'), 'meta.json'), 'w') as metajson:
        metajson.write(json.dumps({
            'guid': env.get_str('guid'),
            'basedir': env.get_str('basedir'),
            'created': dates.datetime_to_unix(datetime.datetime.utcnow()),
            'user': env.get_str('user'),
            'name': env.get_str('name'),
            'dbname': env.get_str('dbname'),
            'port': int(env.get_str('port')),
            'istemp': env.get_bool('scm_istemp'),
            'sandbox_mode': env.get_str('scm_sandbox_mode'),
        }))

def install_basedir_postgresql_files():
    basedir = env.get_str('basedir')
    pgdir = env.get_str('postgresql')
    for subdir in 'bin include lib share'.split():
        subprocess.check_call(['rsync', '-arc', os.path.join(pgdir, subdir), basedir])
        subprocess.check_call(['chmod', '-R', '+w', os.path.join(basedir, subdir)])


def alter_sysem_set(pg, key, value):
    pg.execute('ALTER SYSTEM SET %s = %s;' % (key, value))
