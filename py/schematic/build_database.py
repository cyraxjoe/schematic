import os

from schematic import build_msg, env
from schematic.build_util import build_setup, install_buildinput_files

def main():
    build_setup()
    install_buildinput_files()
    build_msg.send({
        'type': 'nojbohishtim-database-ready',
        'basedir': env.get_str('basedir'),
    })

if __name__ == '__main__':
    main()
