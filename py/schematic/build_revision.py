import json
import os
import sys

import psycopg2

from schematic import env, pgcn
from schematic.build_util import build_setup, install_buildinput_files, apply_sql_file

def revision_exists(pg):
    return pg.execute('''
        SELECT EXISTS (SELECT * FROM pg_tables WHERE schemaname = 'meta' AND tablename = 'revision');
    ''').scalar()

def is_applied(pg, guid):
    return revision_exists(pg) and pg.execute('''
        SELECT EXISTS (SELECT * FROM meta.revision WHERE guid = %(guid)s);
    ''', {'guid': guid}).scalar()

def apply_revision():
    with pgcn.connect(env.get_str('pguri'), autocommit=env.get_bool('autocommit')) as pg:
        upgrade_sql = env.get_str('upgrade_sql')
        if pg.execute('SELECT pg_is_in_recovery();').scalar():
            sys.stdout.write('read-only, skipping %r\n' % upgrade_sql)
            sys.stdout.flush()
            return
        name = env.get_str('name')
        guid = env.get_str('guid')
        if is_applied(pg, guid):
            sys.stderr.write('already applied: %s\n' % name)
            return
        scm_fast_forward = env.get_bool('scm_fast_forward')
        add_meta_revision = env.get_bool('add_meta_revision')
        # add_meta_revision edge case: when fast-forward is requested, we don't want to skip initializing revision
        if scm_fast_forward and add_meta_revision:
            sys.stdout.write('fast-forward %r\n' % upgrade_sql)
            sys.stdout.flush()
        else:
            sys.stdout.write('applying %r\n' % upgrade_sql)
            sys.stdout.flush()
            apply_sql_file(pg, upgrade_sql)
        pg.execute('''
            INSERT INTO meta.revision (guid, name, env, argv, dependencies)
            SELECT
                %(guid)s,
                %(name)s,
                %(env)s,
                %(argv)s,
                %(dependencies)s
        ''', {
            'env': json.dumps(dict(os.environ)),
            'argv': sys.argv,
            'guid': env.get_str('guid'),
            'name': env.get_str('name'),
            'dependencies': env.get_array('dependencies')})
        pg.commit()

def main():
    build_setup()
    sandbox_mode = env.get_str('scm_sandbox_mode', 'imperative')
    if sandbox_mode == 'declarative':
        # creating an "declarative" sandbox database from declarative definitions (instead of revisions)
        pass
    elif sandbox_mode == 'imperative':
        # creating a "imperative" sandbox database from revisions
        install_buildinput_files()
        apply_revision()

if __name__ == '__main__':
    main()
