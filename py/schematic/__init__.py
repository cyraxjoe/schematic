import os
import sys
import uuid

from humbug.consent import HumbugConsent, environment_variable_opt_out, no
from schematic import env

bugsnag_key = env.get_str('BUGSNAG')
if bugsnag_key:
    import bugsnag
    project_root = env.get_str('ROOT_DIR', os.getcwd())
    bugsnag.configure(api_key=bugsnag_key, project_root=project_root)

bugout_token = env.get_str('BUGOUT_TOKEN')
bugout_journal_id = env.get_str('BUGOUT_KB_ID')
bugout_consent = HumbugConsent(environment_variable_opt_out('BUGOUT_CONSENT', no))

if all([bugout_token, bugout_journal_id, bugout_consent]):
    from humbug.report import Reporter

    reporter = Reporter(
        'schematic',
        bugout_consent,
        bugout_token=bugout_token,
        bugout_journal_id=bugout_journal_id
    )

    original_excepthook = sys.excepthook

    def excepthook(type_, instance, traceback):
        reporter.error_report(instance)
        if original_excepthook:
            original_excepthook(type_, instance, traceback)

    sys.excepthook = excepthook
