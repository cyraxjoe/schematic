#! /usr/bin/env python3

# pylint: disable=invalid-name
import copy
import datetime
import gzip
import json
import os
import random
import re
import string
import sys
import tempfile
from collections import namedtuple
from dataclasses import dataclass
from urllib.parse import urlparse

import boto3
import psycopg2
import sh
from psycopg2.sql import SQL, Identifier


@dataclass
class OplogFile:
    bucket: str
    path: str
    name: str
    subdir: str
    tsn: str
    table: str
    size: str
    modified: datetime.datetime

def parse_oploguri(oploguri):
    '''
    >>> parse_oploguri('s3://example/foo.jsonl.gz')  # pylint: disable=line-too-long
    OplogFile(bucket='example', path='foo.jsonl.gz', name='foo.jsonl.gz', subdir='', tsn=None, table=None, size=None, modified=None)
    >>> parse_oploguri('s3://example/bar/foo.jsonl.gz')  # pylint: disable=line-too-long
    OplogFile(bucket='example', path='bar/foo.jsonl.gz', name='foo.jsonl.gz', subdir='bar', tsn=None, table=None, size=None, modified=None)
    >>> parse_oploguri('s3://example/playstore/0000A01CBADCBF28.jsonl.gz')  # pylint: disable=line-too-long
    OplogFile(bucket='example', path='playstore/0000A01CBADCBF28.jsonl.gz', name='0000A01CBADCBF28.jsonl.gz', subdir='playstore', tsn='0000A01CBADCBF28', table=None, size=None, modified=None)
    >>> parse_oploguri('s3://example/playstore/00009FF539AF5667-public.play_app.jsonl.gz')  # pylint: disable=line-too-long
    OplogFile(bucket='example', path='playstore/00009FF539AF5667-public.play_app.jsonl.gz', name='00009FF539AF5667-public.play_app.jsonl.gz', subdir='playstore', tsn='00009FF539AF5667', table='public.play_app', size=None, modified=None)
    >>> parse_oploguri('s3://example/00009FF539AF5667-public.play_app.jsonl.gz')  # pylint: disable=line-too-long
    OplogFile(bucket='example', path='00009FF539AF5667-public.play_app.jsonl.gz', name='00009FF539AF5667-public.play_app.jsonl.gz', subdir='', tsn='00009FF539AF5667', table='public.play_app', size=None, modified=None)
    '''
    uri = urlparse(oploguri)
    assert uri.scheme == 's3'
    path = uri.path.lstrip('/') if uri.path else uri.path
    name = os.path.basename(path) if path else None
    subdir = os.path.dirname(path) if path else None
    tsn = re.match(r'(\w+/)?(?P<tsn>[0-9A-F]{16})[\.-].*', path) if path else None
    if tsn:
        tsn = tsn.group('tsn')
    table = re.match(r'(\w+/)?[0-9A-F]{16}\-(?P<table>\w+\.\w+)\.*', path) if path else None
    if table:
        table = table.group('table')
    return OplogFile(uri.netloc, path, name, subdir, tsn, table, None, None)

def lsn_to_tsn(lsn):
    ''' Encode a textual LSN to 16 digit hex value TSN.
    >>> lsn_to_tsn('0/17E2C48')
    '00000000017E2C48'
    >>> lsn_to_tsn('8925/38C3A4C0')
    '0000892538C3A4C0'
    >>> lsn_to_tsn('A/01234567')
    '0000000A01234567'
    >>> lsn_to_tsn('A/1234567')
    '0000000A01234567'
    >>> lsn_to_tsn(None)
    '''
    return ''.join(map(lambda x: x.rjust(8, '0'), lsn.split('/'))) if lsn else None

def tsn_to_lsn(tsn):
    ''' Encode a 16 digit hex value TSN to a textual LSN.
    >>> tsn_to_lsn('00000000017E2C48')
    '00000000/017E2C48'
    >>> tsn_to_lsn('0000892538C3A4C0')
    '00008925/38C3A4C0'
    >>> tsn_to_lsn('0000000A01234567')
    '0000000A/01234567'
    >>> tsn_to_lsn('0000000A01234567')
    '0000000A/01234567'
    >>> tsn_to_lsn(None)
    '''
    return ('%s/%s' % (tsn[0:8], tsn[8:16])) if tsn else None

def ls(path):
    ''' List available oplog files.
        :path: eg s3://some-bucket/some-prefix. Lists all files following the path specified.
    '''
    parsed = parse_oploguri(path)
    s3 = boto3.client('s3')
    resp = s3.list_objects_v2(Bucket=parsed.bucket, StartAfter=parsed.path, Prefix=parsed.subdir or '')
    for k in resp.get('Contents', []):
        olfile = parse_oploguri(os.path.join('s3://', resp['Name'], k['Key']))
        olfile.modified = k['LastModified']
        olfile.size = k['Size']
        yield olfile

def read_jsonlgz(fileobj):
    for line in gzip.GzipFile(fileobj=fileobj):
        yield json.loads(line)

def get_table_sym(table):
    ''' Get a psycopg2 Identifier symbol given a table dict. '''
    return Identifier(table['namespace'], table['tablename'])

PreparedStatementKey = namedtuple('PreparedStatementKey', ('opname', 'namespace', 'tablename', 'cols'))

def gen_prep_name(prep_key, alphabet=string.digits + string.ascii_uppercase, seed=None):
    ''' Generate a name to use for a prepared statement.
    >>> gen_prep_name(PreparedStatementKey('insert', 'foo', 'bar', ('id', 'name', 'created_at')), seed=0)
    'insert_foo_bar_OQ2GWVPJUMDW'
    '''
    random.seed(seed) if seed is not None else None
    guid = ''.join(random.choice(alphabet) for _ in range(12))
    return '%s_%s_%s_%s' % (prep_key.opname, prep_key.namespace, prep_key.tablename, guid)

def get_cols(op, reckey='new'):
    ''' Get tuple of columns names leading with primary key followed by sorted remaining columns. '''
    keycols = op['table']['key']
    return tuple(keycols + sorted(c for c in op[reckey].keys() if c not in keycols))

def op_insert(cur, op, _prep_cache={}):  # pylint: disable=dangerous-default-value
    ''' Perform an INSERT op. '''
    rec = op['new']
    cols = get_cols(op, reckey='new')
    prep_key = PreparedStatementKey('insert', op['table']['namespace'], op['table']['tablename'], cols)
    if prep_key in _prep_cache:
        prep_name = _prep_cache[prep_key]
    else:
        prep_name = _prep_cache[prep_key] = gen_prep_name(prep_key)
        cur.execute(SQL('''
            PREPARE {prep_name} AS
            INSERT INTO {tbl} ({cols})
            VALUES ({args});
        ''').format(
            prep_name=Identifier(prep_name),
            tbl=get_table_sym(op['table']),
            cols=SQL(', ').join([Identifier(c) for c in cols]),
            args=SQL(', ').join([SQL('$%d' % i) for i, _ in enumerate(cols, start=1)]),
        ))
    cur.execute(
        SQL('EXECUTE {prep_name} %(vals)s;').format(prep_name=Identifier(prep_name)),
        {'vals': tuple(rec[c] for c in cols)})

def op_update(cur, op, _prep_cache={}):  # pylint: disable=dangerous-default-value
    ''' Perform an UPDATE op. '''
    rec = op['new']
    keycols = op['table']['key']
    cols = get_cols(op, reckey='new')
    prep_key = PreparedStatementKey('update', op['table']['namespace'], op['table']['tablename'], cols)
    if prep_key in _prep_cache:
        prep_name = _prep_cache[prep_key]
    else:
        prep_name = _prep_cache[prep_key] = gen_prep_name(prep_key)
        cur.execute(SQL('''
            PREPARE {prep_name} AS
            UPDATE {tbl}
            SET ({cols}) = ({args})
            WHERE ({pkey}) = ({pkey_args});
        ''').format(
            prep_name=Identifier(prep_name),
            tbl=get_table_sym(op['table']),
            cols=SQL(', ').join([Identifier(c) for c in cols]),
            pkey=SQL(', ').join([Identifier(c) for c in keycols]),
            args=SQL(', ').join([SQL('$%d' % i) for i, _ in enumerate(cols, start=1)]),
            pkey_args=SQL(', ').join([SQL('$%d' % (i + len(cols))) for i, _ in enumerate(keycols, start=1)]),
        ))
    keyrec = op['old'] or op['new']  # primary key might change, match on old key
    keyvals = tuple(keyrec[c] for c in keycols)
    cur.execute(
        SQL('EXECUTE {prep_name} %(vals)s;').format(prep_name=Identifier(prep_name)),
        {'vals': tuple(rec[c] for c in cols) + keyvals})

def op_upsert(cur, op, _prep_cache={}):  # pylint: disable=dangerous-default-value
    ''' Perform an INSERT or UPDATE op ("upsert") using PostgreSQL ON CONFLICT clause[1].
        The WHERE clause is an optimization to eliminate a write when nothing has changed.
        The PREPARE statement[2] is an optimization that saves repetitive query parsing and planning, as well as
        reducing query size over the wire.

        [1] https://www.postgresql.org/docs/current/sql-insert.html
        [2] https://www.postgresql.org/docs/current/sql-prepare.html
    '''
    rec = op['new']
    cols = get_cols(op, reckey='new')
    keycols = op['table']['key']
    prep_key = PreparedStatementKey('upsert', op['table']['namespace'], op['table']['tablename'], cols)
    if prep_key in _prep_cache:
        prep_name = _prep_cache[prep_key]
    else:
        prep_name = _prep_cache[prep_key] = gen_prep_name(prep_key)
        cur.execute(SQL('''
            PREPARE {prep_name} AS
            INSERT INTO {tbl} ({cols})
            VALUES ({args})
            ON CONFLICT ({pkey}) DO UPDATE SET
                ({cols}) = ROW({args})
            WHERE ({qualcols}) IS DISTINCT FROM ({args});
        ''').format(
            prep_name=Identifier(prep_name),
            tbl=get_table_sym(op['table']),
            cols=SQL(', ').join([Identifier(c) for c in cols]),
            qualcols=SQL(', ').join([Identifier(op['table']['tablename'], c) for c in cols]),
            pkey=SQL(', ').join([Identifier(c) for c in keycols]),
            args=SQL(', ').join([SQL('$%d' % i) for i, _ in enumerate(cols, start=1)]),
        ))
    # When a non-nullable value is omitted (because it is TOASTed and hasn't changed), then this function will fail
    # the null constraint check on insert; use savepoint to recover and fallback to op_update instead.
    if op['op'] == 'update':
        cur.execute('SAVEPOINT upsert_sp;')
    try:
        cur.execute(
            SQL('EXECUTE {prep_name} %(vals)s;').format(prep_name=Identifier(prep_name)),
            {'vals': tuple(rec[c] for c in cols)})
    except psycopg2.errors.NotNullViolation:
        if op['op'] == 'update':
            cur.execute('ROLLBACK TO SAVEPOINT upsert_sp;')
            return op_update(cur, op)
        raise
    if op['op'] == 'update':
        cur.execute('RELEASE SAVEPOINT upsert_sp;')
    if 'old' in op:
        oldrec = op['old']
        if not oldrec:
            return
        try:
            oldkey = [oldrec[c] for c in keycols]
        except KeyError:
            return
        newkey = [rec[c] for c in keycols]
        if oldkey != newkey:  # if the primary key has changed, we also need to delete the old record
            delop = copy.deepcopy(op)
            delop['op'] = 'delete'
            op_delete(cur, delop)

def op_delete(cur, op, _prep_cache={}):  # pylint: disable=dangerous-default-value
    ''' Perform a DELETE op. '''
    rec = op['old']
    keycols = op['table']['key']
    prep_key = PreparedStatementKey('delete', op['table']['namespace'], op['table']['tablename'], tuple(keycols))
    if prep_key in _prep_cache:
        prep_name = _prep_cache[prep_key]
    else:
        prep_name = _prep_cache[prep_key] = gen_prep_name(prep_key)
        cur.execute(SQL('''
            PREPARE {prep_name} AS
            DELETE FROM {tbl}
            WHERE ({pkey}) = ({pkey_args});
        ''').format(
            prep_name=Identifier(prep_name),
            tbl=get_table_sym(op['table']),
            pkey=SQL(', ').join([Identifier(c) for c in keycols]),
            pkey_args=SQL(', ').join([SQL('$%d' % i) for i, _ in enumerate(keycols, start=1)]),
        ))
    cur.execute(
        SQL('EXECUTE {prep_name} %(vals)s;').format(prep_name=Identifier(prep_name)),
        {'vals': tuple(rec[c] for c in keycols)})

def op_truncate(cur, op):
    ''' Perform a TRUNCATE op. '''
    statement = 'TRUNCATE {tbls}'
    if op['cascade']:
        statement += ' CASCADE'
    if op['restartid']:
        statement += ' RESTART IDENTITY'
    cur.execute(SQL(statement).format(tbls=SQL(', ').join([get_table_sym(table) for table in op['tables']])))

def read_oplog(olfile):
    tmpdir = os.environ.get('TMPDIR') or os.path.expandvars('$HOME/var/tmp')
    try:
        sh.mkdir('-p', tmpdir)
    except sh.ErrorReturnCode_1 as ex:
        print('failed to create temp directory: %s' % tmpdir, file=sys.stderr)
        print('hint: set TMPDIR env var use a different temp directory', file=sys.stderr)
        raise
    with tempfile.TemporaryFile(dir=tmpdir) as opfp:
        boto3.client('s3').download_fileobj(olfile.bucket, olfile.path, opfp)
        opfp.seek(0)
        for op in read_jsonlgz(opfp):
            yield op

def ingress_file(conn, olfile, start_tsn, initcopy_batch=100000):
    ''' Download oplog file and concurrently process ops as writes into the database.

        For higher performance (reducing round-trips to database), consider using `execute_batch`:
        https://www.psycopg.org/docs/extras.html#fast-execution-helpers

        Params:
        :initcopy_batch: When doing an (idempotent) initial copy that's potentially large, commit batches of this size
            instead of a single large transaction. Pass None to disable.
    '''
    cur = conn.cursor()
    def commit(tsn, tts):
        cur.execute('SELECT pg_replication_origin_xact_setup(%s, %s);', (tsn_to_lsn(tsn), tts))
        conn.commit()
    txn = tsn = tts = None
    def logstatus(i, txn, tsn, tts):
        print('wrote %10d ops, txn %s, tsn %s, tts %s' % (i, txn, tsn, tts), file=sys.stderr)
    i = 0
    for i, op in enumerate(read_oplog(olfile), start=1):
        tsn, tts = op['tsn'], op['tts']
        if start_tsn and tsn <= start_tsn:
            continue  # already written this op, skip
        if txn and txn != op['txn']:
            commit(tsn, tts)
        elif olfile.table and initcopy_batch and (i + 1) % initcopy_batch == 0:
            conn.commit()
        txn = op['txn']
        if op['op'] == 'insert':
            op_upsert(cur, op)  # or: op_insert(cur, op) ; upsert is safer if there's existing data
        elif op['op'] == 'update':
            op_update(cur, op)
        elif op['op'] == 'delete':
            op_delete(cur, op)
        elif op['op'] == 'truncate':
            op_truncate(cur, op)
        if i % 10000 == 0:
            logstatus(i, txn, tsn, tts)
    logstatus(i, txn, tsn, tts)
    commit(tsn, tts)

def ingress(s3uri, pguri, replication_role='replica'):
    ''' Receive oplog update feed into postgresql.
        :s3uri: eg s3://some-bucket/some-prefix
        :pguri: eg postgresql://user:pass@localhost:port/dbname?application_name=oplog-ingress
        :replication_role: specifies PostgreSQL's session_replication_role setting[1];
            use 'replica' to disable triggers including foreign key constraint validation (requires superuser);
            use 'origin' to enable triggers and foreign key constraint validation

        [1] https://www.postgresql.org/docs/current/runtime-config-client.html#RUNTIME-CONFIG-CLIENT-STATEMENT
    '''

    assert s3uri, 's3uri is required'
    subid = re.match(r's3://[\w-]+/\w+/', s3uri)
    assert subid, 'unable to parse subscription id from s3uri'
    subid = subid.group(0)
    conn = psycopg2.connect(pguri)
    cur = conn.cursor()

    # tables with self-referential foreign keys may cause constraint violation if rows are out of dependency order
    # this defers constraint validation until the end of the transaction
    # https://www.postgresql.org/docs/current/sql-set-constraints.html
    cur.execute('SET CONSTRAINTS ALL DEFERRED;')
    assert replication_role in ('replica', 'origin', 'local'), 'invalid replication_role %r' % replication_role
    if replication_role != 'origin':
        try:
            cur.execute('SET session_replication_role = %s;', (replication_role,))
        except psycopg2.errors.InsufficientPrivilege:  # pylint: disable=no-member
            print('replication role %s requires superuser' % replication_role, file=sys.stderr)
            print((
                'hint: replication role "replica" is faster because triggers and foreign key constraints are skipped, '
                'but replication role "origin" can be used without superuser access.'), file=sys.stderr)
            sys.exit(1)
    # disabling synchronous_commit improves performance and is safe in this context because upon a postgresql crash
    # recent writes can be lost, but we'll just replay them anyway on the next run
    cur.execute('SET SESSION synchronous_commit TO OFF;')
    cur.execute('SELECT pg_try_advisory_lock(-1699317507, hashtext(%s));', (subid,))
    lock = cur.fetchone()[0]
    if not lock:
        print('there is another running process consuming the subscription %s' % subid, file=sys.stderr)
        raise SystemExit(1)
    cur.execute('SELECT coalesce(pg_replication_origin_oid(%s), pg_replication_origin_create(%s));', (subid, subid))
    cur.execute('SELECT pg_replication_origin_session_setup(%s);', (subid,))
    cur.execute('SELECT pg_replication_origin_session_progress(false);')
    start_tsn = lsn_to_tsn(cur.fetchone()[0])
    start_uri = os.path.join(os.path.dirname(s3uri), start_tsn) if start_tsn else s3uri
    for olfile in ls(start_uri):
        print('oplog file %12.d bytes\t %s \t %s' % (olfile.size, olfile.modified, olfile.path), file=sys.stderr)
        ingress_file(conn, olfile, start_tsn)
