import json
import re
import sys

msg_call_sign = 'gadyeapcatlyophnoafhewvagsitjeno'

def send(data):
    assert 'type' in data, 'message requires type field'
    assert re.match(r'[a-z]{12}\-[a-z\-]+', data['type']), 'invalid message type'
    sys.stdout.write('%s %s\n' % (msg_call_sign, json.dumps(data)))
    sys.stdout.flush()

def read(line):
    match = re.match(r'%s (?P<msg>.+)\n' % msg_call_sign, line)
    if match:
        msg = json.loads(match.groupdict()['msg'])
        return msg
