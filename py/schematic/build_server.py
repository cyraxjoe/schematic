'''
Build a new postgresql database server.
'''
import os
import subprocess

import psycopg2

from schematic import env, pgcn
from schematic.build_util import (
    build_setup, request_pg_start, mkdir, write_meta_json, install_basedir_postgresql_files, alter_sysem_set)

def create_database(basedir, port, user, dbname):
    conn = psycopg2.connect(env.get_str('pguri_postgres'))
    cursor = conn.cursor()
    cursor.execute('''SELECT * FROM pg_database WHERE datname = %(dbname)s;''', vars={'dbname': dbname})
    database = cursor.fetchone()
    if not database:
        subprocess.check_call([
            os.path.join(basedir, 'bin/createdb'), '-p', port, '-U', user, dbname,
            '--encoding', 'utf8', '--locale', 'C'])

def main():
    build_setup()
    port = env.get_str('port')
    dbname = env.get_str('dbname')
    user = env.get_str('user')
    basedir = env.get_str('basedir')
    mkdir(basedir)
    write_meta_json()
    install_basedir_postgresql_files()
    datadir = env.get_str('datadir')
    if not os.path.exists(datadir):
        with open('pwfile', 'w') as fpointer:
            fpointer.write(env.get_str('password'))
        initdb_args = [
            os.path.join(basedir, 'bin/initdb'), '-D', datadir, '-U', user, '--pwfile', 'pwfile',
            '--encoding', 'utf8', '--locale', 'C']
        if env.get_bool('scm_istemp'):
            initdb_args += ['--no-sync']
        subprocess.check_call(initdb_args)
        os.remove('pwfile')
        subprocess.check_call(['chmod', '-R', '0700', datadir])
        with open(os.path.join(datadir, 'postgresql.conf'), 'a') as fpointer:
            fpointer.write('\nport = %s\n' % port)
    request_pg_start(basedir, port)
    create_database(basedir, port, user, dbname)
    with pgcn.connect(env.get_str('pguri'), autocommit=True) as pg:
        alter_sysem_set(pg, 'port', port)
        if env.get_bool('scm_istemp'):
            alter_sysem_set(pg, 'fsync', 'off')
            pg.execute('SELECT pg_reload_conf();')
    install_basedir_postgresql_files()

if __name__ == '__main__':
    main()
