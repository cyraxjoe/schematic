import json
import os
import re
import subprocess
import sys

import psycopg2

from schematic import env, pgcn
from schematic.build_util import build_setup, install_buildinput_files, apply_sql_file

def main():
    build_setup()
    name = env.get_str('name')
    sandbox_mode = env.get_str('scm_sandbox_mode', 'imperative')
    if sandbox_mode == 'declarative':
        # creating an "declarative" sandbox database from declarative definitions (instead of revisions)
        install_buildinput_files()
        upgrade_sql = env.get_str('upgrade_sql')
        sys.stdout.write('applying %r\n' % upgrade_sql)
        sys.stdout.flush()
        with pgcn.connect(env.get_str('pguri')) as pg:
            apply_sql_file(pg, upgrade_sql)
            pg.commit()
    elif sandbox_mode == 'imperative':
        # creating a "imperative" sandbox database from revisions
        pass

if __name__ == '__main__':
    main()
