'''
PostgreSQL URIs.

See: http://www.postgresql.org/docs/current/static/libpq-connect.html#LIBPQ-CONNSTRING
'''

import os
import json
from collections import namedtuple

pguri_base = namedtuple('PgURI', ('user', 'password', 'host', 'port', 'dbname', 'params'))
pguri_base.__new__.__defaults__ = (None, None, None, None, None, None)

class PgURI(pguri_base):

    def __str__(self):
        uri = 'postgresql://'
        uri += self.user or ''
        uri += ':%s' % self.password if self.user and self.password else ''
        uri += '@' if self.user else ''
        uri += self.host or ''
        uri += ':%s' % self.port if self.port else ''
        uri += '/%s' % self.dbname if self.dbname else ''
        uri += ('?%s' % '&'.join('%s=%s' % (k, v) for (k, v) in self.params.iteritems()) if self.params else '')
        return uri

def pguri_from_basedir(basedir):
    metajson = os.path.join(basedir, 'meta.json')
    metadata = json.loads(open(metajson).read())
    return PgURI(user=metadata['user'], host='localhost', port=metadata['port'], dbname=metadata['dbname'])
