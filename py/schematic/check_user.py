'''
Test that a user exists.
'''
from schematic import pgcn, env
from schematic.build_util import build_setup

def main():
    build_setup()
    username = env.get_str('username')
    with pgcn.connect(env.get_str('pguri')) as pg:
        user = pg.execute('''
            SELECT * FROM pg_user WHERE usename = %(username)s;
        ''', {'username': username}).fetchone()
        assert user is not None, 'user not found! %r' % username
        req_super = env.get_bool('super')
        if req_super is not None:
            assert req_super == user.usesuper, 'superuser test expected %r, got %r' % (req_super, user.usesuper)

if __name__ == '__main__':
    main()
