#!/usr/bin/env bash

# Autohook
# A very, very small Git hook manager with focus on automation
# Contributors:   https://github.com/Autohook/Autohook/graphs/contributors
# Version:        2.3.0
# Website:        https://github.com/Autohook/Autohook

# MIT License
# 
# Copyright (c) 2017 Nikola Kantar, 2021 Scott Milliken
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.


# USAGE

# Run `.githooks/autohook.sh install` to install.
# Place hooks at `.githooks/<HOOK TYPE>/<NAME>` and be sure to chmod +x the file.
# Script should return non-zero exit code for error, 0 for success.

resetline() {  # set insertion point to start of line so it can overwrite status instead of appending
    echo -e -n '\r\e[1A\e[K'
}

setstatus() {
    echo "[autohook.sh] $(date +%k:%M:%S) $*";
}

install() {
    hook_types=(
        "applypatch-msg"
        "commit-msg"
        "post-applypatch"
        "post-checkout"
        "post-commit"
        "post-merge"
        "post-receive"
        "post-rewrite"
        "post-update"
        "pre-applypatch"
        "pre-auto-gc"
        "pre-commit"
        "pre-push"
        "pre-rebase"
        "pre-receive"
        "prepare-commit-msg"
        "update"
    )

    repo_root=$(git rev-parse --show-toplevel)
    hooks_dir="$repo_root/.git/hooks"
    autohook_linktarget="../../.githooks/autohook.sh"
    for hook_type in "${hook_types[@]}"
    do
        hook_symlink="$hooks_dir/$hook_type"
        ln -sf "$autohook_linktarget" "$hook_symlink"
    done
}

errorlog="/tmp/git-hook-error.log"

main() {
    calling_file=$(basename "$0")

    if [[ $calling_file == "autohook.sh" ]]
    then
        command=$1
        if [[ $command == "install" ]]
        then
            install
        fi
    else
        repo_root=$(git rev-parse --show-toplevel)
        hook_type=$calling_file
        symlinks_dir="$repo_root/.githooks/$hook_type"
        files=("$symlinks_dir"/*)
        number_of_symlinks="${#files[@]}"
        if [[ $number_of_symlinks == 1 ]]
        then
            if [[ "$(basename "${files[0]}")" == "*" ]]
            then
                number_of_symlinks=0
            fi
        fi
        if [[ $number_of_symlinks -gt 0 ]]
        then
            setstatus "$number_of_symlinks hooks"
            for file in "${files[@]}"
            do
                test -x "$file" || continue  # skip non-executable files
                scriptname=$(basename "$file")
                resetline ; setstatus "$scriptname"
                eval "\"$file\"" &> $errorlog
                exit_code="$?"
                if [[ $exit_code != 0 ]]
                then
                    resetline ; setstatus "$scriptname ERROR $exit_code"
                    cat "$errorlog"
                    exit $exit_code
                fi
            done
            resetline
        fi
    fi
}


main "$@"
