DO $$ BEGIN
    IF NOT EXISTS (SELECT * FROM pg_tables WHERE schemaname = 'meta' AND tablename = 'revision') THEN
        CREATE SCHEMA IF NOT EXISTS meta;
        SET LOCAL search_path = meta;
        CREATE TABLE revision (
            guid text not null,
            name text not null,
            date timestamp not null default now(),
            env jsonb not null,
            argv text[] not null,
            dependencies text[] not null
        );
        ALTER TABLE revision ADD CONSTRAINT revision_pk PRIMARY KEY (guid);
        CREATE INDEX revision_name_idx ON revision (name);
        CREATE INDEX revision_date_idx ON revision (date);
    END IF;
END $$;


