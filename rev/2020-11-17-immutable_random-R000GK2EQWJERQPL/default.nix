stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:

scm.revision {
    guid = "R000GK2EQWJERQPL";
    name = "2020-11-17-immutable_random";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2020-11-16-immutable_random-R000GHY0WOEAEDLN>
    ];
}
