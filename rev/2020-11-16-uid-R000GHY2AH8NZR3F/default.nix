stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:

scm.revision {
    guid = "R000GHY2AH8NZR3F";
    name = "2020-11-16-uid";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2020-11-16-smallint_bit-R000GHY1QOY4QC9J>
    ];
}
