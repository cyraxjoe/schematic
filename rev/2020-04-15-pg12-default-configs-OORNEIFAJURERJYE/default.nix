stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:

scm.revision {
    guid = "OORNEIFAJURERJYE";
    name = "2020-04-15-pg12-default-configs";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
