stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:

scm.revision {
    guid = "R000GHY0WOEAEDLN";
    name = "2020-11-16-immutable_random";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
