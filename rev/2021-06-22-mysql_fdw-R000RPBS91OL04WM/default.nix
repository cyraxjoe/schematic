stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:

scm.revision rec {
    guid = "R000RPBS91OL04WM";
    name = "2021-06-22-mysql_fdw";
    upgrade_sql = ./upgrade.sql;
    dependencies = [ ];
    buildInputs = [
      (pkgs.callPackage ./extension.nix (stdargs // {inherit name guid;}) )
    ];
}
