stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:

scm.revision {
    guid = "DISKYALJIBASOCAC";
    name = "2020-03-23-pg_repack";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
    buildInputs = [
        (pkgs.callPackage ./extension.nix {})
    ];
}
