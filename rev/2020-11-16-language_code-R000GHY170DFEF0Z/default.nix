stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:

scm.revision {
    guid = "R000GHY170DFEF0Z";
    name = "2020-11-16-language_code";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
