stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:

scm.revision {
    guid = "R000GN1I735GUAX5";
    name = "2020-11-19-intarray";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
}
