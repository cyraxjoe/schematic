stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:

scm.revision {
    guid = "PYIVEKDOCEOSHJOT";
    name = "2020-03-24-pg_libphonenumber";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        
    ];
    buildInputs = [
        (pkgs.callPackage ./extension.nix stdargs)
    ];
}
