stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:

scm.revision {
    guid = "R000QSCKEW5MBKWP";
    name = "2021-06-04-country";
    upgrade_sql = ./upgrade.sql;
    autocommit = true;
    dependencies = [
        <2020-11-16-country-R000GHY2HI7C7675>
    ];
}
