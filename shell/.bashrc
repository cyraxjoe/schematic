#! /usr/bin/env bash
HISTLOGPATH="$HOME/.history/$(hostname)/$(date -u +%Y/%m)"
HISTLOGFILE=$HISTLOGPATH/$(date -u +%dd.%Hh.%Mm.%Ss)_bash-$$
mkdir -p $HISTLOGPATH
export SCM_VAR="$HOME/var"
mkdir -p "$SCM_VAR"
export SCM_PG="$SCM_VAR/pg"
mkdir -p "$SCM_PG"
export SCM_TMUX="$SCM_VAR/tmux"
mkdir -p "$SCM_TMUX"
export TMUX_TMPDIR="$SCM_TMUX"
export SCM_PATH="$(realpath "$SCM_PATH")"
if [ ! -z "$shellHook" ]; then
    # we just built a new nix-env, cache it for later use
    export ROOT_DIR=$PWD
    unset shellHook
    export ENV_FILE="$ROOT_DIR"/.schematic.env
    export -p > $ENV_FILE
    $SCM_PATH/.githooks/autohook.sh install  # install git hooks
else
    # we are entering a nix-env from a plain bash env or an old nix-env that should be refreshed
    cached_env=$(ls .schematic.env 2>/dev/null || printf "")
    [ -n "$cached_env" ] && source $cached_env && export ENV_FILE="$cached_env" && cd $ROOT_DIR
    unset latest_env
fi
chmod +x $SCM_PATH/shell/.bashrc $SCM_PATH/shell/nixbash $SCM_PATH/shell/.bash_shopt $SCM_PATH/shell/.bash_aliases $SCM_PATH/.githooks/autohook.sh
export PATH=$SCM_PATH/shell:$PATH
export PYTHONPATH=$SCM_PATH/py
export PYTHONPYCACHEPREFIX="$SCM_VAR/.pycache"
export SCM_REPOS="$SCM_REPOS:$SCM_PATH"
export NIX_PATH="$(scm mknixpath)"
export PS1='\[\033[01;91m\]($name)\[\033[00m\] \[\033[01;32m\]\u@\h\[\033[00m\]:\[\033[01;34m\]\w\[\033[00m\]\$ '
unset TZ
export SHELL="$SCM_PATH/shell/nixbash"
export HISTCONTROL=ignoredups:ignorespace
export HISTFILESIZE=1000000000                  # max lines in bash history file
export HISTSIZE=1000000000                      # max lines in bash history per session
export EDITOR=vim
[ -f /etc/bash_completion ] && ! shopt -oq posix && source /etc/bash_completion
[ -x /usr/bin/lesspipe ] && eval "$(/usr/bin/lesspipe)" # enable less to view non-textual files
[ -x /usr/bin/dircolors ] && eval "$(/usr/bin/dircolors -b)" # enable color support
[ -f $SCM_PATH/shell/.bash_aliases ] && source $SCM_PATH/shell/.bash_aliases
[ -f $SCM_PATH/shell/.bash_shopt ] && source $SCM_PATH/shell/.bash_shopt
for completion_file in $(ls $SCM_PATH/shell/.bash_completions/*); do
    source $completion_file
done
export TERM=screen-256color
export PG_COLOR=auto
export PSQL_HISTORY="$HISTLOGFILE""-psql"
export IPYTHON_HISTORY="$HISTLOGFILE""-ipython"
export IPYTHONDIR=$ROOT_DIR/.ipython/
promptFunc(){
    history -a  # flush to history file
    # audit log of all bash commands
    echo "$(date +%Y-%m-%d--%H-%M-%S) $PWD $(history 1)" >> $HISTLOGFILE
}
PROMPT_COMMAND=promptFunc
