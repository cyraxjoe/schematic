{ pkgs
, stdenv
, buildPythonPackage
, six
, sqlalchemy
, sqlbag
}: buildPythonPackage rec {
    name = "schemainspect-0.1.1610933302";
    src = pkgs.fetchurl {
        url = "https://files.pythonhosted.org/packages/c7/d3/7b49ccc4b2c45e3642dbde17e99de3c31c9ce0522da8ee5f249cc82881c0/schemainspect-0.1.1610933302.tar.gz";
        sha256 = "6a5653f953826f4f1d72ead3300069537e58c0a96a388f52787ef11532805505";
    };
    propagatedBuildInputs = [
        six
        sqlalchemy
        sqlbag
    ];
}
