{ pkgs
, stdenv
, buildPythonPackage
, six
, sqlbag
, schemainspect
}: buildPythonPackage rec {
    name = "migra-1.0.1610499506";
    src = pkgs.fetchurl {
        url = "https://files.pythonhosted.org/packages/2c/5d/08b804858770cf3e3647e3a4bf719bd20ed052523e4478dc667040009780/migra-1.0.1610499506.tar.gz";
        sha256 = "47a98afb33b3d243dbf953112502117fd3f2dd72117536a8148b79831fd31cc4";
    };
    propagatedBuildInputs = [
        six
        schemainspect
        sqlbag
    ];
}
