args @ { pkgs, SCM_PATH, repos, verbose ? false }:

let stdenv = pkgs.stdenv; in
pkgs.mkShell rec {
    name = "schematic";
    inherit SCM_PATH;
    SCM_VERBOSE = if verbose then "1" else "0";
    SCM_REPOS = builtins.concatStringsSep ":" args.repos;
    shellHook = ''
        source ${SCM_PATH}/shell/.bashrc
    '';
    py3 = pkgs.python38.override {
        packageOverrides = self: super: with self; {
            autodebug = (callPackage ./autodebug.nix {});
            bugsnag = (callPackage ../lib/bugsnag.nix {});
            bugout = (self.callPackage ../lib/bugout.nix {});
            humbug = (self.callPackage ../lib/humbug.nix {});
            locust = (callPackage ./locust.nix {});
            migra = (callPackage ./migra.nix {});
            pglast = (callPackage ./pglast.nix {});
            pydantic = (callPackage ../lib/pydantic.nix {});
            schemainspect = (callPackage ./schemainspect.nix {});
            sqlbag = (callPackage ./sqlbag.nix {});
            typing-extensions = (callPackage ../lib/typing-extensions.nix {});
        };
    };
    buildInputs = (with pkgs; [
        (git.override { openssh = openssh_with_kerberos; })
        bashInteractive
        broot
        curl
        glibcLocales
        gnugrep
        hostname
        htop
        jq
        less
        man
        nix-prefetch-scripts
        postgresql
        s3cmd
        shellcheck
        tig
        tmux
        tree
        which
        (py3.buildEnv.override {
            ignoreCollisions = true;
            extraLibs = with py3.pkgs; [
                (if stdenv.isLinux then autodebug else null)
                (if stdenv.isLinux then pylint else null)
                Fabric
                boto
                boto3
                bugsnag
                click
                clint
                ipython
                isort
                humbug
                locust
                migra
                nose
                pglast
                psutil
                psycopg2
                pudb
                requests
                schemainspect
                setproctitle
                sh
                sqlalchemy
                sqlbag
                tkinter
                toolz
            ];
        })
    ] ++ (if stdenv.isLinux then [
        pgadmin
        pg_top
    ] else []));
}
