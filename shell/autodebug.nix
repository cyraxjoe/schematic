{ pkgs
, stdenv
, buildPythonPackage
}: buildPythonPackage rec {
    name = "autodebug-1.0.1";
    src = pkgs.fetchurl {
        url = "https://pypi.python.org/packages/source/a/autodebug/autodebug-1.0.1.tar.gz";
        sha256 = "02n2c3ccjk6cp7scvfixbniaa2jfcai0dm403i612xyqhgbjmyrb";
    };
}
