{ lib
, buildPythonPackage
, fetchFromGitHub
, isPy3k
, pythonOlder
, setuptools
, aenum
, pytest
, pytestcov
, perl
, bison
, flex
, postgresql_12
}:

let postgresql = postgresql_12; in  # TODO: support other postgresql versions
buildPythonPackage rec {
  pname = "pglast";
  version = "1.12";
  src = fetchFromGitHub {
    owner  = "rdunklau";
    repo   = "pglast";
    rev    = "49d177f044b8094bd5df347e0999e96d9f3cfad9";
    sha256 = "14dgv29yqq49cldwfmrwwam87qppkg22xlv5yzk9b9mpa6smplfq";
    fetchSubmodules = true;
  };

  disabled = !isPy3k;

  nativeBuildInputs = [ perl bison flex ];
  propagatedBuildInputs = [ setuptools ] ++ lib.optionals (pythonOlder "3.6") [ aenum ];

  checkInputs = [ pytest pytestcov ];

  prePatch = ''
    substituteInPlace libpg_query/Makefile --replace "PG_VERSION = 12.1" "PG_VERSION = ${postgresql.version}"
    substituteInPlace libpg_query/Makefile --replace "curl -o \$(PGDIRBZ2) https://ftp.postgresql.org/pub/source/v\$(PG_VERSION)/postgresql-\$(PG_VERSION).tar.bz2" "cp ${postgresql.src.outPath} \$(PGDIRBZ2)"
  '';
  checkPhase = ''
    pytest
  '';

  meta = with lib; {
    homepage = "https://github.com/rdunklau/pglast.git";
    description = "PostgreSQL Languages AST and statements prettifier";
    license = licenses.gpl3Plus;
    maintainers = [ maintainers.marsam ];
  };
}
