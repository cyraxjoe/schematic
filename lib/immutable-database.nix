stdargs @ { scm, pkgs, stdenv, postgresql, python3, rsync, ... }:
args @ { upgrade_sql ? null, dependencies ? [], postgresql, ... }:

scm.effect rec {
    exec = ../py/schematic/build_immutable_database.py;
    inherit postgresql;
    name = "sandbox";
    dbname = "sandbox";
    host = "localhost";
    port = builtins.getEnv "SCM_PORT";
    user = builtins.getEnv "SCM_USER";
    password = builtins.getEnv "SCM_PASS";
    pguri = "postgresql://${user}:${password}@${host}:${port}/${dbname}?application_name=scm";
    pguri_postgres = "postgresql://${user}:${password}@${host}:${port}/postgres?application_name=scm";
    buildInputs = [
        rsync
    ];
    propagatedBuildInputs = (map (module: (import module { inherit scm server; })) (scm.deps dependencies)) ++ [
        postgresql
        python3
    ];
    inherit upgrade_sql;
}
