let
    pkgs = (import ./nixpkgs.nix { });
    stdenv = pkgs.stdenv;
    python3 =
        let _python3 = pkgs.python38.override {
            packageOverrides = self: super: with self; {
                bugsnag = (callPackage ./bugsnag.nix {});
                bugout = (self.callPackage ./bugout.nix {});
                humbug = (self.callPackage ./humbug.nix {});
                pydantic = (self.callPackage ./pydantic.nix {});
                typing-extensions = (self.callPackage ./typing-extensions.nix {});
            };
        }; in _python3.buildEnv.override {
        extraLibs = with _python3.pkgs; [
            ipython
            psycopg2
            pudb
            sqlalchemy
            toolz
            bugsnag
            humbug
        ];
    };
    stdargs = { inherit scm pkgs stdenv python3; postgresql = pkgs.postgresql; };
    scm = rec {
        effect = pkgs.callPackage ./effect.nix stdargs;
        server = pkgs.callPackage ./server.nix stdargs;
        replica = pkgs.callPackage ./replica.nix stdargs;
        remoteServer = pkgs.callPackage ./remote-server.nix stdargs;
        database = pkgs.callPackage ./database.nix stdargs;
        immutableDatabase = pkgs.callPackage ./immutable-database.nix stdargs;
        revision = pkgs.callPackage ./revision.nix stdargs;
        schema = pkgs.callPackage ./schema.nix stdargs;
        checkUser = pkgs.callPackage ./check-user.nix stdargs;
        callmod = module: pkgs.callPackage module stdargs;
        deps = dependencies: (map (x: (pkgs.callPackage x stdargs)) dependencies);
        buildDeps = server: self:
            # don't use revisions if we're in "declarative" mode
            let depends = builtins.filter (x: server.scm_sandbox_mode != "declarative" || x.scm_type != "revision") self.depends; in
            let depInputs = (map (x: x.apply { self = x; inherit server; }) depends); in
            [ (server.apply { self = server; }) ] ++ self.propagatedBuildInputs ++ self.buildInputs ++ depInputs;
    };
in scm
