stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:

let
    reverseString = str: with pkgs.lib; concatStrings (reverseList (stringToCharacters str));
    db_modpath = (/. + (builtins.getEnv "SCM_DATABSE_MODULE"));
    ff_modpath = (/. + (builtins.getEnv "SCM_FAST_FORWARD_MODULE"));
    db = (import db_modpath stdargs);
    ff = (import ff_modpath stdargs);
in (scm.database rec {
    guid = builtins.getEnv "SCM_FAST_FORWARD_GUID";
    name = ''${db.name}-fast-forward'';
    server = db.server;
    dependencies = if ff.scm_type == "database" then ff.dependencies else [ ff_modpath ];
    propagatedBuildInputs = if ff.scm_type == "database" then ff.propagatedBuildInputs else [];
    buildInputs = if ff.scm_type == "database" then ff.buildInputs else [];
})
