stdargs @ { scm, pkgs, stdenv, python3, ... }:
{ server, username, super ? false }:

scm.effect rec {
    inherit (server) postgresql pguri;
    inherit username super;
    name = "checkUser-${username}";
    exec = ../py/schematic/check_user.py;
}
