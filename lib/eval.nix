{ modulepath }:
let
    pkgs = (import ./nixpkgs.nix { });
    scm = (import ./default.nix);
    database = pkgs.callPackage modulepath { inherit scm; };
in database.apply { server = database.server; self = database; }
