{ lib
, buildPythonPackage
, fetchPypi
, bugout
}:

buildPythonPackage rec {
  pname = "humbug";
  version = "0.1.12";

  src = fetchPypi {
    inherit pname version;
    sha256 = "1g89yb2njj755q4fr229gw6pgbaayyxsdkizmh0dfsrg6zmb1szd";
  };

  propagatedBuildInputs = [ bugout ];

  # no tests
  doCheck = false;

  meta = with lib; {
    description = "The Humbug Python library";
    homepage = "https://github.com/bugout-dev/humbug";
    license = licenses.asl20;
    platforms = platforms.unix;
  };
}

