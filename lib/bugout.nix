{ lib
, buildPythonPackage
, fetchPypi
, requests
, pydantic
}:

buildPythonPackage rec {
  pname = "bugout";
  version = "0.1.5";

  src = fetchPypi {
    inherit pname version;
    sha256 = "1rrdnic9jvfkgcd6slh5wbwpgpl3ly28pzn5d55pkhcgz3jsb15v";
  };

  propagatedBuildInputs = [ requests pydantic ];

  # no tests
  doCheck = false;

  meta = with lib; {
    description = "Python client library for Bugout API";
    homepage = "https://github.com/bugout-dev/bugout-python";
    license = licenses.mit;
    platforms = platforms.unix;
  };
}


