{ modulepath }:
let
    pkgs = (import ./nixpkgs.nix { });
    scm = (import ./default.nix);
    showMod = p: let m = (scm.callmod p); in {
        inherit (m) guid scm_type name;
        store_path = p;
        source_path = toString p;
        basefiles = toString m.basefiles;
        dependencies = map showMod m.dependencies;
    } // ((m.inspect or (self: {})) m);
in scm.effect {
    exec = ../py/schematic/build_inspect.py;
    name = "inspect-${builtins.getEnv "SCM_RAND"}";
    output = builtins.toFile ((builtins.baseNameOf modulepath) + ".json") (builtins.toJSON (showMod modulepath));
}
