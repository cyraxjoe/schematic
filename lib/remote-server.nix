stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:
{ postgresql, host, port, name, user, password }:

{
    inherit postgresql host port name user password;
    pguri = "postgresql://${user}:${password}@${host}:${port}/${name}?application_name=scm";
}
