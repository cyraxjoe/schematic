{
    rev ? "43a775649ce6c8a6d2b1d7bea8ccccd6eac72468",
    # obtain with nix-prefetch-url --unpack "https://github.com/nixos/nixpkgs/archive/${rev}.tar.gz"
    sha256 ? "04iyjiy309a0h2kfwfvi7qmgsvixg2yjrdmds5x5gr0nkvhikhvf",
    overlays ? []
}:
(import (builtins.fetchTarball{ name = "nixpkgs-${builtins.substring 0 7 rev}";
                                url = "https://github.com/nixos/nixpkgs/archive/${rev}.tar.gz";
                                inherit sha256;})
  {
    overlays = [
        (self: super: {
            postgresql = self.postgresql_12;
        })
    ] ++ overlays ++ [
        (self: super: {
            postgresql = super.postgresql.overrideAttrs (super: rec {
                version = "12.6";
                psqlSchema = "12";
                sha256 = "028asz92mi3706zabfs8w9z03mzyx62d1l71qy9zdwfabj6xjzfz";
                src = builtins.fetchurl {
                    url = "https://ftp.postgresql.org/pub/source/v${version}/${super.pname}-${version}.tar.bz2";
                    inherit sha256;
                };
                # remove this patch that changes socket dir from /tmp to /run/postgresql since the latter requires root
                patches = (builtins.filter (x: (builtins.baseNameOf x) != "socketdir-in-run.patch") super.patches);

                # This overrides producing multiple store paths (out, lib, doc, man) instead of just one.
                # Multiple store paths breaks pg_config behavior where it will output directories relative to the
                # binary that was invoked (supporting relocating binaries, which we need). In particular,
                # `pg_config --libdir` would point to the nix store path instead of our libdir, breaking support for
                # installing extensions which require a mutable libdir. This also fixes `pg_config --docdir`,
                # `pg_config --htmldir`, and `pg_config --pkglibdir` output.
                outputs = [ "out" ];
                configureFlags = (builtins.filter (x: x != "--libdir=$(lib)/lib") super.configureFlags) ++ [
                    # "--with-llvm"
                    "--with-python"
                    "--with-perl"
                    "--with-tcl"
                    "--with-segsize=16"
                    "--with-blocksize=8"
                ];
                buildInputs = super.buildInputs ++ (with self; [
                    # clang
                    # llvm
                    perl
                    python
                    tcl
                ]);
            });
        })
    ];
})
