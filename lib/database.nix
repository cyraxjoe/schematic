stdargs @ { scm, pkgs, stdenv, postgresql, python3, rsync, ... }:
args @ { guid, name, server, dependencies ? [], propagatedBuildInputs ? [], buildInputs ? [], basefiles ? null, ... }:

{
    scm_type = "database";
    inherit guid name dependencies server propagatedBuildInputs buildInputs basefiles;
    depends = scm.deps dependencies;
    apply = { self, server }: scm.effect rec {
        inherit (server) postgresql pguri basedir port name;
        inherit (self) basefiles;
        exec = ../py/schematic/build_database.py;
        buildInputs = [ rsync ] ++ (scm.buildDeps server self);
    };
    inspect = self: {
        server = self.server.inspect server;
    };
}
