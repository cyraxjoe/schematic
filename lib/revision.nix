stdargs @ { scm, pkgs, stdenv, postgresql, python3, rsync, ... }:
args @ {
    guid,
    name,
    dependencies ? [],
    propagatedBuildInputs ? [],
    buildInputs ? [],
    basefiles ? null,
    upgrade_sql,
    add_meta_revision ? true,
    autocommit ? false,
    ...
}:

{
    scm_type = "revision";
    inherit guid name dependencies upgrade_sql propagatedBuildInputs buildInputs basefiles add_meta_revision autocommit;
    depends = scm.deps dependencies;
    apply = { self, server }: scm.effect rec {
        inherit (server) postgresql pguri basedir scm_sandbox_mode;
        inherit (self) guid upgrade_sql name add_meta_revision basefiles autocommit;
        scm_fast_forward = builtins.getEnv "SCM_FAST_FORWARD";
        exec = ../py/schematic/build_revision.py;
        dependencies = map (a: a.guid) (builtins.filter (a: a ? guid) self.depends);
        # revisions have an implied dependency on revision to log that they've been applied
        buildInputs = [ rsync ] ++ (scm.buildDeps server (self // { depends = self.depends ++ (if add_meta_revision then scm.deps [ <meta.revision-ADTUKDUCMEEMEGPI> ] else []); }));
    };
}
