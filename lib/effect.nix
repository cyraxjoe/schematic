stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:
args @ { exec, ... }:

derivation (rec {
    inherit stdenv;
    inherit (stdenv) system;
    inherit (pkgs) bash;
    builder = "${python3}/bin/python3";
    args = [ exec ];
    PYTHONPATH = ../py;
} // args)
