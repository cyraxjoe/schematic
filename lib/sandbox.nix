stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:

let
    reverseString = str: with pkgs.lib; concatStrings (reverseList (stringToCharacters str));
    modpath = (/. + (builtins.getEnv "SCM_SANDBOX_MODULE"));
    mod = (import modpath stdargs);
in (scm.database rec {
    guid = reverseString server.guid;
    name = ''${pkgs.lib.maybeEnv "SCM_NAME" "sandbox"}'';
    server = scm.server rec {
        inherit postgresql;
        guid = builtins.getEnv "SCM_GUID";
        name = ''${pkgs.lib.maybeEnv "SCM_NAME" "sandbox"}'';
        dbname = ''${pkgs.lib.maybeEnv "SCM_NAME" "sandbox"}'';
        port = builtins.getEnv "SCM_PORT";
        user = pkgs.lib.maybeEnv "SCM_USER" "root";
        password = pkgs.lib.maybeEnv "SCM_PASS" "pass";
        scm_istemp = pkgs.lib.maybeEnv "SCM_ISTEMP" true;
        scm_sandbox_mode = pkgs.lib.maybeEnv "SCM_SANDBOX_MODE" "declarative";  # NB: "declarative" populates database using declarative definitions, "imperative" populates database using revisions
    };
    dependencies = if mod.scm_type == "database" then mod.dependencies else [ modpath ];
    propagatedBuildInputs = if mod.scm_type == "database" then mod.propagatedBuildInputs else [];
    buildInputs = if mod.scm_type == "database" then mod.buildInputs else [];
})
