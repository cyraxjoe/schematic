stdargs @ { scm, pkgs, stdenv, postgresql, python3, rsync, ... }:
args @ {
    postgresql,
    guid, name, dbname, port, user, password,
    scm_istemp ? false,
    scm_sandbox_mode ? "imperative",
    cache_buster ? null,
    ...
}:

rec {
    scm_type = "server";
    dependencies = [];
    inherit postgresql guid name dbname port user password scm_istemp scm_sandbox_mode cache_buster;
    scm_pg = builtins.getEnv "SCM_PG";
    host = "localhost";
    basedir = "${scm_pg}/${name}-${guid}";
    datadir = "${basedir}/data";
    pguri = "postgresql://${user}:${password}@${host}:${port}/${dbname}?application_name=scm";
    pguri_postgres = "postgresql://${user}:${password}@${host}:${port}/postgres?application_name=scm";
    apply = { self }: scm.effect rec {
        inherit (self) postgresql guid name dbname port user password scm_istemp scm_sandbox_mode cache_buster;
        inherit (self) scm_type scm_pg host basedir datadir pguri pguri_postgres;
        exec = ../py/schematic/build_server.py;
        buildInputs = [ rsync ];
        propagatedBuildInputs = [
            postgresql
            python3
        ];
    };
    inspect = self: {
        inherit (self) guid name dbname port user password scm_istemp scm_sandbox_mode;
        inherit (self) scm_type scm_pg host basedir datadir pguri pguri_postgres;
    };
}
