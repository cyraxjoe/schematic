stdargs @ { scm, pkgs, stdenv, postgresql, python3, rsync, ... }:
args @ {
    guid,
    name,
    dependencies ? [],
    upgrade_sql,
    propagatedBuildInputs ? [],
    buildInputs ? [],
    basefiles ? null,
    ...
}:

{
    scm_type = "schema";
    inherit guid name dependencies upgrade_sql propagatedBuildInputs buildInputs basefiles;
    depends = scm.deps dependencies;
    apply = { self, server }: scm.effect rec {
        inherit (server) postgresql pguri port basedir scm_sandbox_mode;
        inherit (self) guid upgrade_sql name basefiles;
        exec = ../py/schematic/build_schema.py;
        buildInputs = [ rsync ] ++ (scm.buildDeps server self);
    };
}
