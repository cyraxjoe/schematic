stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:

scm.schema {
    guid = "ADTUKDUCMEEMEGPI";
    name = "meta.revision";
    upgrade_sql = ./upgrade.sql;
    # NB: revisions of the revision schema must define `add_meta_revision = false;` to prevent self-dependency
    dependencies = [
        <2020-03-13-meta.revision-UDEJFOTFOSEOWRAI>
    ];
}
