stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:

scm.schema {
    guid = "FURWIAHEDBAYRIJO";
    name = "pg_libphonenumber";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2020-03-24-pg_libphonenumber-PYIVEKDOCEOSHJOT>
    ];
    buildInputs = [
        (pkgs.callPackage ./extension.nix stdargs)
    ];
}
