stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:

scm.schema {
    guid = "S03PG8N8AKZ0964V";
    name = "smallint_bit";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2020-11-16-smallint_bit-R000GHY1QOY4QC9J>
    ];
}
