{ pkgs, stdenv, postgresql, readline, openssl, zlib, ... }:

stdenv.mkDerivation rec {
    name = "pg_repack-${version}";
    version = "1.4.5";
    src = pkgs.fetchurl {
        url = "https://github.com/reorg/pg_repack/archive/ver_${version}.tar.gz";
        sha256 = "0idm6rqm1ymzkpvjzir1wnyk2sm0nw4zgl4lynqnlw4d27pax097";
    };
    buildInputs = [
        readline
        openssl
        zlib
        postgresql
    ];
    installPhase = ''
        targetdir=$out/basefiles
        install -D bin/pg_repack -t $targetdir/bin/
        install -D lib/pg_repack.so -t $targetdir/lib/
        install -D lib/{pg_repack--${version}.sql,pg_repack.control} -t $targetdir/share/postgresql/extension/
    '';
}
