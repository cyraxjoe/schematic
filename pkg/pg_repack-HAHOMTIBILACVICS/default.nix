stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:

scm.schema {
    guid = "HAHOMTIBILACVICS";
    name = "pg_repack";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2020-03-23-pg_repack-R000GJVYKLW2SCDC>
    ];
    buildInputs = [
        (pkgs.callPackage ./extension.nix {})
    ];
}
