stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:

scm.schema {
    guid = "S0Y03K5GGRJIT4SL";
    name = "intarray";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2020-11-19-intarray-R000GN1I735GUAX5>
    ];
}
