stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:

scm.schema {
    guid = "S0Y2F1PPW4X10VTW";
    name = "country";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <map_hashtext-S0OVX2CXRXVXY93W>
        <intarray-S0Y03K5GGRJIT4SL>
        <2021-06-04-country-R000QSCKEW5MBKWP>
    ];
}
