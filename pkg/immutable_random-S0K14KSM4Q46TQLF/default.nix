stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:

scm.schema {
    guid = "S0K14KSM4Q46TQLF";
    name = "immutable_random";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2020-11-17-immutable_random-R000GK2EQWJERQPL>
    ];
}
