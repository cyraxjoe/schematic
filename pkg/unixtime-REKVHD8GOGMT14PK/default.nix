stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:

scm.schema {
    guid = "REKVHD8GOGMT14PK";
    name = "unixtime";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <2020-04-17-unixtime-D75YK101UONQRBYA>
    ];
}
