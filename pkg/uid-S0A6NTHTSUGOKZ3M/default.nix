
stdargs @ { scm, pkgs, stdenv, postgresql, python3, ... }:

scm.schema {
    guid = "S0A6NTHTSUGOKZ3M";
    name = "uid";
    upgrade_sql = ./upgrade.sql;
    dependencies = [
        <smallint_bit-S03PG8N8AKZ0964V>
        <2020-11-16-uid-R000GHY2AH8NZR3F>
    ];
}
