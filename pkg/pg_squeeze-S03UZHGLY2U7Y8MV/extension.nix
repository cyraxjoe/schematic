{ pkgs, stdenv, postgresql, readline, openssl, zlib, ... }:

stdenv.mkDerivation rec {
    name = "pg_squeeze-1.3.0";
    src = pkgs.fetchurl {
        url = "https://github.com/cybertec-postgresql/pg_squeeze/archive/REL1_3_0.tar.gz";
        sha256 = "0423km42q74f8lqamjcb078mk4l63bjl2kdpw9wxh2k96829fs04";
    };
    buildInputs = [
        postgresql
    ];
    installPhase = ''
        targetdir=$out/basefiles
        mkdir -p "$targetdir/data"
        install -D pg_squeeze.so -t $targetdir/lib/
        install -D {*.sql,*.control} -t $targetdir/share/postgresql/extension/
    '';
}
