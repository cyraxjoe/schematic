args @ {
    repos ? [],
    nixpkgs_overlays ? [],
    verbose ? false
}:
let
    pkgs = (import ./lib/nixpkgs.nix { overlays = nixpkgs_overlays; });
in rec {
    inherit pkgs;
    SCM_PATH = ./.;
    shell = (import ./shell {
        inherit pkgs SCM_PATH verbose repos;
    } );
}
